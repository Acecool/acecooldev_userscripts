//
//  - Josh 'Acecool' Moser
//
// ==UserScript==
// @name			Acecool - ____ - AcecoolWeb_Framework
// @author			Acecool
// @namespace		Acecool
// @version			0.0.1
// @description		Replaces encoded-links with decoded direct-links on episode finder sites.
// @description		Automatically click the "continue" button, and attempt to skip the countdown if any, on video watching sites.
// @description		Remove ad panels on video watching sites.
// @match			http://*/*
// @match			https://*/*
// @require			http://code.jquery.com/jquery-latest.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecool_functions_lib.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecoolweb_framework.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_default.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_steam.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_ebay.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_projects.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_video.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecoolweb_framework_runtime.js
// @run-at			document-start
// @grant			GM_xmlhttpRequest
// @grant			GM_getResourceURL
// @grant			GM_addStyle
// @xrequire			https://gist.github.com/raw/2620135/checkForBadJavascripts.js
// ==/UserScript==


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// BitBucket
//

// Define the var used throughout ( I use _tag to make it easier to copy and paste for new additions, but I do keep TAG_X in the first AddFeature( ) call for my CodeMapper )
var _tag			= InitializeTag( 'BitBucket' );

// As SetupTagHosts returns the _tag enum value, I take this opportunity to set it - you can also define hosts to use, but as this is the default it acts as a fallthrough so everything runs through this...
const TAG_BITBUCKET	= SetupTagHosts( _tag, 'bitbucket.org' );

// Define configuration key / values for this tag... You can use one per line, or set them up in sequence...
SetupTagConfig( _tag, 'ProjectSrcPathElementID', 'source-container', 'LinkModFileTypeIcon', false, 'LinkModDivide', ' ', 'LinkModPrefix', '•', 'LinkModSuffix', '' );

// Setup Tasks: Preload, OnReady, OnLoad, OnFrameLoad, SetupObserver
SetupTasks( _tag, false, true, false, false, false );

// Setup Processes: Anchors, Divs, Frames, Observer
SetupProcesses( _tag, true, false, false, true );

//
// Helper - Called immediately as this script is executed on the page..
// TASK_ON_PRELOAD -- Note: For some reason it isn't working on Preload any longer... Bitbucket possibly altered site logic...
// Works TASK_ON_LOAD but takes some time for first load..
//
AddFeature( TAG_BITBUCKET, TASK_ON_DOCUMENT_READY, function( _data, _cfg )
{
	// Using the host makes things easier if the domain changes, or if another site we frequent uses the same url-scheme.
	// https://bitbucket.org/Acecool/acecooldev_framework/src/e975eeea7460?at=master
	// https://bitbucket.org/Acecool/acecooldev_framework/src/master/acecooldev_framework/_assets/?at=master
	// Set up our Pattern - Comment: captured groups....1........................2...........................3........4............
	//X var _pattern = new RegExp( '^https?:\/\/' + Acecool.util.GetHost( ) + '(\/[^/]+\/[^/]+)\/(src|raw|history-node|diff)\/([0-9a-f]{40}|[0-9a-f]{12})?\/?(.*)$', 'ig');
	//X var _pattern = new RegExp( '^https?:\/\/' + Acecool.util.GetHost( ) + '(\/[^/]+\/[^/]+)\/(src|raw|history-node|diff)\/([0-9a-f]{40}|[0-9a-f]{12})?\/?(.*)$', 'ig');
	//X var _pattern = new RegExp( '^https?:\/\/' + Acecool.util.GetHost( ) + '(\/[^/]+\/[^/]+)\/(src|raw|history-node|diff)\/([0-9a-f]{40}|[0-9a-f]{12})\/(.*)$', 'ig');
	//Y var _pattern = new RegExp( '^https?:\/\/' + Acecool.util.GetHost( ) + '(\/[^/]+\/[^/]+)\/(src|raw|diff)\/([0-9a-f]{40}|[0-9a-f]{12})\/(.*)$', 'ig');
	//Y var _history_pattern = new RegExp( '[0-9a-f]{7}', 'ig');

	// Set up the patterns we'll be using... ( Updated November 2, 2017 by adding ? after {12})\/ so it'd match /src/123456789012?at=master for the first breadcrumb url )
	_cfg.SearchPattern = new RegExp( '^https?:\/\/' + Acecool.util.GetHost( ) + '(\/[^/]+\/[^/]+)\/(src|raw|diff)\/([0-9a-f]{40}|[0-9a-f]{12})\/?(.*)$', 'ig');
	_cfg.DirPattern = new RegExp( '[0-9a-f]{7}', 'ig');

	// Output to console as seeing something in another light can help with mistakes; only needed for debugging...
	if ( ACECOOL_DEBUG_MODE )
		__Log( "[ Acecool - BitBucket ] Dynamic Master Direct-Linking: " + _cfg.SearchPattern );

	// Preprocess the links right away - This happens automatically ON_PRELOAD now that we have it set to use TASK_PROCESS_ANCHORS...
	// SiteSpecifcFeatures[ GetTaskFuncName( TAG_BITBUCKET, 'ReplaceLinks' ) ]( _data, _cfg );

	// Set up the configuration for our observers
	var _config = { attributes: true, childList: true, characterData: true };

	// Monitor source-containor div for changes and update links on change... '#source-list', '#repo-content', '#content', '#source-path'
	var _container = GetCfg( 'ProjectSrcPathElementID', 'source-container', '#' );
	var _node = document.querySelector( _container );

	// Monitor changes to source-container...
	RunTime.GetObserver( ).observe( _node, _config );
} );


//
// Helper - Called on anchor on the page for each of the non Process / Monitor Tasks if exists and if MAP_SITE_TASKS Process Anchors is True..
//
AddFeature( _tag, TASK_PROCESS_ANCHORS, function( _anchor, _cfg )
{
	//
	// For every anchor on the page...
	//

	//
    var _pattern = _cfg.SearchPattern;
    var _history_pattern = _cfg.DirPattern;
	// Since we're matching many different aspects, lets check the path that matters ( src vs raw vs history-node vs diff ) so we can take alternate actions 1
	var _dir = _anchor.href.replace( _pattern, "$2" );

	// Link / URL
	var _link = _anchor.href;

	// The text the user sees - Note: HTML is stripped with .textContent and leading / trailing whitespace is trimmed.
	var _title = _anchor.textContent.trim( ); // Full Data: .innerHTML; // Custom Library Function: .StripHTML( );

	// All HTML Removed title - this is so we can add our own icons, etc...
	// var _clean = _anchor.innerHTML.StripHTML( );

	// If we're in history mode, we need links to remain the same... The title of the link will be 7 chars mix of numbers / letters.. if we match then ignore...
	if ( ( _dir == "history-node" ) || ( _title.length == 7 && _title.match( _history_pattern ) ) )
		return;

	// The link for differences checker is different so we need to modify it differently and prevent the other code from running..
	if ( _dir == "diff" )
	{
		// Explode the anchor... by ?
		var _parts = _link.split( "?" );

		// Make sure we aren't modifying any of the anchors ( other than primary )
		var _anchors = _parts[ 1 ].href.split( "#" );

		// The URL without the anchor
		var _url_part = ( _anchors[ 0 ].length > 0 ) ? _anchors[ 0 ]: _parts[ 0 ];

		// The anchor
		var _anchor_part = ( _anchors[ 1 ].length > 0 ) ? "#" + _anchors[ 1 ]: '';

		// Set up the link to use a cleaner method...
		_anchor.href = _part + "?diff2=master&amp;at=master" + _anchor_part;

		// We don't want to modify the link twice, so we'll stop here..
		return;
	}

	// Only replace links which match the pattern and that haven't been modified already ( detected above )..
	if ( _link.match( _pattern ) )
	{
		// We've matched an anchor to our pattern, now we re-use some elements ( in parens as $1, 2, 3, and 4 ) to rebuild the link and put it together.
		var _url = _link.replace( _pattern, "$1/$2/master/$4" );

		// Output to console so we have some record as to what happened, and for debugging purposes ( this doesn't need to occur unless debugging as io can be expensive )..
		if ( ACECOOL_DEBUG_MODE )
			__Log( "[ BitBucket Direct-Link ] " + _url );

		// Update the anchor url / link with our rebuilt url linking to master instead of an ugly hash in the url...
		_anchor.href = _url;

		// Keep the original...
		var _title_new = _title;

		// If we are adding a prefix, add it..
		if ( _cfg.LinkModPrefix !== '' )
			_title_new = _cfg.LinkModPrefix + _cfg.LinkModDivide + _title_new;

		// If we are adding a suffix, add it..
		if ( _cfg.LinkModSuffix !== '' )
			_title_new = _title_new + _cfg.LinkModDivide + _cfg.LinkModSuffix;

		// If you enable the file-type icon to appear, then innerHTML is modified to remove the original, otherwise the title is modified...
		if ( _cfg.LinkModFileTypeIcon )
			_anchor.innerHTML = '[ICON]' + _title_new;
		else
			_anchor.innerHTML = _anchor.innerHTML.replace( _title, _title_new );
	}
} );


//
// Helper - Observer calls this function on Mutation - Force call Process Anchors when our observer runs the callback..
//
AddFeature( _tag, TASK_ON_OBSERVER_MUTATION_CALLBACK, function( _data, _cfg )
{
	// SiteSpecifcFeatures[ GetTaskFuncName( TAG_BITBUCKET, 'ReplaceLinks' ) ]( _data, _cfg );
	__main_ultimate_site_manager_script__( TASK_PROCESS_ANCHORS );
} );



	// console.log( ' >> Container: ' + _container );

	// var _node = document.querySelectorAll( _container )[ 0 ];
	// var _node = document.getElementById( _container );
	// var _nodes = document.getElementsByTagName( "div" );


	// console.log( ' >> Containers: ' + _nodes.length );

	// var _node = null;
	// for ( var _i = 0; _i < _nodes.length; _i++ )
	// {
	// 	_node = _nodes[ _id ];

	// 	if ( _node.id == 'source-container' )
	// 	{
	// 		console.log( ' >> FOUND SOURCE-CONTAINER!!!' );
	// 		break;
	// 	}

	// }
	// var _node = $( _container ).eq( 0 );
	// var _node = $( _container ).first( );
	// var _node = $( _container ).last( );
	// console.log( ' >> Node from querySelector: ' + _node );


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// BitBucket
//

// Define the var used throughout ( I use _tag to make it easier to copy and paste for new additions, but I do keep TAG_X in the first AddFeature( ) call for my CodeMapper )
var _tag			= InitializeTag( 'GitHub' );

// As SetupTagHosts returns the _tag enum value, I take this opportunity to set it - you can also define hosts to use, but as this is the default it acts as a fallthrough so everything runs through this...
const TAG_GITHUB	= SetupTagHosts( _tag, 'github.com' );

// Define configuration key / values for this tag... You can use one per line, or set them up in sequence...
// SetupTagConfig( _tag, 'LinkModFileTypeIcon', false, 'LinkModDivide', ' ', 'LinkModPrefix', '•', 'LinkModSuffix', '' );

// Setup Tasks: Preload, OnReady, OnLoad, OnFrameLoad, SetupObserver
SetupTasks( _tag, true, false, false, false, false );

// Setup Processes: Anchors, Divs, Frames, Observer
SetupProcesses( _tag, false, false, false, false );

//
// Helper - Called immediately as this script is executed on the page..
//
AddFeature( TAG_GITHUB, TASK_ON_PRELOAD, function( _data, _cfg )
{

} );


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//