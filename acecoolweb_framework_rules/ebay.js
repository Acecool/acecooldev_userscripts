//
//  - Josh 'Acecool' Moser
//
// ==UserScript==
// @name			Acecool - ____ - AcecoolWeb_Framework
// @author			Acecool
// @namespace		Acecool
// @version			0.0.1
// @description		Replaces encoded-links with decoded direct-links on episode finder sites.
// @description		Automatically click the "continue" button, and attempt to skip the countdown if any, on video watching sites.
// @description		Remove ad panels on video watching sites.
// @match			http://*/*
// @match			https://*/*
// @require			http://code.jquery.com/jquery-latest.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecool_functions_lib.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecoolweb_framework.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_default.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_steam.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_ebay.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_video.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecoolweb_framework_runtime.js
// @run-at			document-start
// @grant			GM_xmlhttpRequest
// @grant			GM_getResourceURL
// @grant			GM_addStyle
// @xrequire			https://gist.github.com/raw/2620135/checkForBadJavascripts.js
// ==/UserScript==


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// eBay
//

// Define the var used throughout ( I use _tag to make it easier to copy and paste for new additions, but I do keep TAG_X in the first AddFeature( ) call for my CodeMapper )
var _tag			= InitializeTag( 'eBay' ); //, 'ebay.com'

// As SetupTagHosts returns the _tag enum value, I take this opportunity to set it - you can also define hosts to use, but as this is the default it acts as a fallthrough so everything runs through this...
const TAG_EBAY		= SetupTagHosts( _tag, 'cart.payments.ebay.com' );

// Define configuration key / values for this tag... You can use one per line, or set them up in sequence...
SetupTagConfig( _tag, 'anchor_id', 0, 'cart_regex', new RegExp( 'sc\/([a-z]+)\\?', 'i') );

// Setup Tasks: Preload, OnReady, OnLoad, OnFrameLoad, SetupObserver
SetupTasks( _tag, true, false, false, false, false );

// Setup Processes: Anchors, Divs, Frames, Observer
SetupProcesses( _tag, true, false, false, false );

//
// Templates for all of the current tasks...
// Note: Finish eBay
//


//
//
//
const EBAY_CART_URL						= "https://cart.payments.ebay.com/sc/view";
const RESOURCE_IMAGE_LOADING_WHEEL		= "https://secureir.ebaystatic.com/cr/v/c1/ColwebImages/throbber_15px.gif";


//
// eBay Cart URLs - for matching purposes...
//

//
const URL_MATCH_PREFIX					= "/sc/";

// Remove from Save for later list ==	/sc/rfsfl
const URL_MATCH_REMOVE_SAVED			= "rfsfl";

// Save for later list > Add to cart == /sc/sflatc
const URL_MATCH_ADD_SAVED				= "sflatc";

// Remove from cart ==	/sc/rfc
const URL_MATCH_REMOVE					= "rfc";

// Save for later ==	/sc/sfl
const URL_MATCH_SAVE					= "sfl";

// Pay only this seller ==	/sc/pots
const URL_MATCH_PAY_SELLER				= "pots";

// Proceed to checkout ==	/sc/ptc
const URL_MATCH_CHECKOUT				= "ptc";


//
// Language MAP for the ShoppingCart URLs..
//
var L_URL_MATCH_MAP = { };
L_URL_MATCH_MAP[ URL_MATCH_PREFIX ]			= "Shopping Cart";
L_URL_MATCH_MAP[ URL_MATCH_REMOVE_SAVED ]	= "Remove from Save for later list";
L_URL_MATCH_MAP[ URL_MATCH_ADD_SAVED ]		= "Add to cart from Save for later list";
L_URL_MATCH_MAP[ URL_MATCH_REMOVE ]			= "Remove from cart";
L_URL_MATCH_MAP[ URL_MATCH_SAVE ]			= "Save for later";
L_URL_MATCH_MAP[ URL_MATCH_PAY_SELLER ]		= "Pay only this seller";
L_URL_MATCH_MAP[ URL_MATCH_CHECKOUT ]		= "Proceed to checkout";


//
// Language - Generic
//

//
const L_ERROR							= " [ Error - Retry ]";
const L_WORKING							= " [ Working {0} ]";


//
// Note: For below, we no longer use L_BUTTON_ for searching...
//

//
// The text we use to search for in all anchors ( update this to your language ) - This for for the Save for later link...
// var _a = __HandleAddingOnClickFunctionality( _anchor, L_BUTTON_SAVE, L_SAVING, L_SAVED, L_ERROR_SAVING, L_CART_MANIPULATE_SUCCESS ); //L_CART_ITEM_SAVED_SUCCESS
//
const L_BUTTON_SAVE						= "Save for later";
const L_SAVING							= " [ Saving {0} ]";
const L_SAVED							= " [ Saved ] ";
const L_ERROR_404						= " [ Error downloading background page ]";
const L_ERROR_SAVING					= " [ Error saved item to list - item is still in your cart ]";

// Search to see whether or not the item was saved for later..
const L_SUCCESS_SAVE					= "has been saved for later.";
//const L_CART_ITEM_SAVE_SUCCESS		= "You can find it in the saved items section below your shopping cart.";


//
// The text we use to search for in all anchors ( update this to your language ) - This for for the Remove link...
// var _a = __HandleAddingOnClickFunctionality( _anchor, L_BUTTON_REMOVE, L_REMOVING, L_REMOVED, L_ERROR_REMOVING, L_CART_MANIPULATE_SUCCESS ); //L_CART_ITEM_REMOVE_SUCCESS
//
const L_BUTTON_REMOVE					= "Remove";
const L_REMOVING						= " [ Removing {0} ]";
const L_REMOVED							= " [ Removed ] ";
const L_ERROR_REMOVING					= " [ Error removing item from cart ]";

// Search to see whether or not the item was removed from the cart..
const L_SUCCESS_REMOVE					= "was removed from your cart.";


//
// The text we use to search for in all anchors ( update this to your language ) - This for for the Saved Items Add back to cart link...
// var _a = __HandleAddingOnClickFunctionality( _anchor, L_BUTTON_ADD, L_ADDING, L_ADDED, L_ERROR_ADDING, L_CART_MANIPULATE_SUCCESS ); //L_CART_ITEM_ADD_SUCCESS );
//
const L_BUTTON_ADD						= "Add back to cart";
const L_ADDING							= " [ Adding {0} ]";
const L_ADDED							= " [ Added ] ";
const L_ERROR_ADDING					= " [ Error adding item back to cart ]";

//
const L_SUCCESS_ADD						= "has been added to your cart.";


//
// Search Terms:
//

//
const L_SUCCESS_UPDATE					= '<i class="gspr icsu"></i>';





//
// Variables, ENUMeration, CONSTants, etc...
//

// Is debugging mode enabled ( ie should we output console.log messages - print output is an expensive operation so this should be left as false unless you're modifying the code and need to understand what is going on )?
const ACB_DEBUG_MODE				= false;

// What should we use as a divide between the Pre/Postfix symbol( s ) to make it look cleaner ( We use space here because some links are trimmed by BitBucket )?
const ACB_LINK_MODIFIER_DIVIDE	  = " ";

// What should we add to the start of a link if it is modified ( Avoid using spaces as the first character[ s ] because Bitbucket trims some of their links )?
const ACB_LINK_MODIFIER_PREFIX	  = "";
const ACB_LINK_MODIFIER_PREFIX_LEN  = ACB_LINK_MODIFIER_PREFIX.length;

// What should we add to the end of a link if it is modified ( Avoid using spaces as the first character[ s ] because Bitbucket trims some of their links )?
const ACB_LINK_MODIFIER_POSTFIX	 = "*";
const ACB_LINK_MODIFIER_POSTFIX_LEN = ACB_LINK_MODIFIER_POSTFIX.length;

//
// ( 1 = Init, 2 = Unknown, 3 = Progress Callback, 4 = Completed )
//
const HTTP_READYSTATE_INIT = 1;
const HTTP_READYSTATE_UNKNOWN = 2;
const HTTP_READYSTATE_PROGRESS_CALLBACK = 3;
const HTTP_READYSTATE_COMPLETED = 4;


//
//
//
const ANCHOR_STATE_WAITING = 0;
const ANCHOR_STATE_FINISHED = 1;



//
// OnClick Algorithm for when the user clicks on Remove from cart...
//
// @arg: _anchor - The link the user linked should be passed in in object form..
// @arg: _div - The complete div encapsulating the cart-entry
// @arg: _parent - The div encapsulating the User / Company you're buying from ( Important so if last item is removed, this is removed too - or if it needs to update the pay only this seller button, etc... then it can do that too )
//
function __CartRemove( _anchor, _div, _parent )
{
	// The Anchor URL / .http which is used to remove the item from the cart...
	var _url = _anchor.href;

	// Add div overlay so we know that the div is being modified....
	_div.background = "#FF0000";

	// Run the http.Get
	__http( _url, function( _data, _status, _xhr ) {

	}, function( _error, _status, _xhr ) {

	} );
}


//
//
//
function __CreateLoadingIMG( _anchor )
{
	//
	_img = document.createElement( 'img' );
	_img.setAttribute( 'src', RESOURCE_IMAGE_LOADING_WHEEL );

	//someElement.parentNode.insertBefore(newElement, someElement.nextSibling);
	// _anchor.parentNode.insertBefore( _img, _anchor.nextSibling );
	_img.insertAfter( _anchor );

	return _img;
}


//
//
//
function __CreatePipe( _anchor )
{
	//
	var _text = "|";
	_border = document.createElement( 'span' );
	_border.setAttribute( 'class', "mr10 ml10 linkClr" );
	_border.defaultInnerHTML = _text;
	_border.innerHTML = _text;

	//someElement.parentNode.insertBefore(newElement, someElement.nextSibling);
	// _anchor.parentNode.insertBefore( _border, _anchor.nextSibling );
	_border.insertAfter( _anchor );

	return _border;
}


//
//
//
function __CreateAnchor( _anchor, _text, _name, _url )
{

	//
	var _border = __CreatePipe( _anchor );

	//
	_a = document.createElement( 'a' );
	_a.href = _url;
	_a.name = _name;
	_a.defaultInnerHTML = _text;
	_a.innerHTML = _a.defaultInnerHTML;

	//someElement.parentNode.insertBefore(newElement, someElement.nextSibling);
	// _border.parentNode.insertBefore( _a, _border.nextSibling );
	_a.insertAfter( _border );


	return _a;
}


//
//
//
function __HandleLink( _anchor, _name )
{
	//_anchor.url = _anchor.href;
	//_anchor.href = "#";

	// Create a "refresh cart" link...
	if ( _anchor.textContent === L_BUTTON_SAVE )
		var _ar = __CreateAnchor( _anchor, "▲", "Reload Cart", EBAY_CART_URL );

	// Create the Anchor we'll use for the AJAX features ( needed because the default link we'll use later )
	var _a = __CreateAnchor( _anchor, _name, _name.Clean( ) + "_" + _anchor.__unique_id, "#" + _name.Clean( ) + "_" + _anchor.__unique_id );

	// Let the user know we've modified the links and the script is ready for use..
	_anchor.textContent = "* ";

	// Return the new anchor
	return _a;
}


//
//
//
function __TextRepeatBounce( _anchor, _max )
{
	// Setup default vars..
	if ( !_anchor.__direction || !_anchor.__count )
	{
		_anchor.__direction = 1;
		_anchor.__count = 0;
	}

	// Controlled toggle - If traveling up and we've reached the max, then change direction.. If traveling down and at 0, change...
	if ( ( _anchor.__direction > 0 && _anchor.__count >= _max ) || ( _anchor.__direction < 0 && _anchor.__count <= 0 ) )
		_anchor.__direction = _anchor.__direction * -1;

	// Update the count...
	_anchor.__count = _anchor.__count + ( 1 * _anchor.__direction );

	//
	return _anchor.__count;
}


//
//
//
function __UpdateTextDotDotDot( _anchor, _text )
{
	// How many dots to show...\
	var _count = __TextRepeatBounce( _anchor, 3 );

	// How many dots to show...
	var _dots = ( _count > 0 ) ? ".".repeat( _count ) : "";

	// If we're no longer in the waiting state, then don't update the text...
	if ( _anchor.state == ANCHOR_STATE_WAITING )
	{
		// Update the text
		_anchor.textContent = _text.replace( "{0}", _dots ); //_text.format( "{0}", _dots );

		// Repeat...
		setTimeout( function( ) {
			__UpdateTextDotDotDot( _anchor, _text );
		}, 500 );
	}
}


//
//
//
function __UpdateAnchorText( _anchor, _text, _text_alt, _count, _max )
{
	var _counter = _count + 1;

	//
	_anchor.textContent = _text + " " + _counter;

	if ( _text_alt )
	{
		setTimeout( function( )
		{
			_anchor.textContent = _text_alt + " " + _counter;

			//
			if ( _count < 3 )
				setTimeout( function( ) {
					__UpdateAnchorText( _anchor, _text, _text_alt, _counter );
				}, 500 );
		}, 500 );
	}
}


//
//
//
function __HandleOnSuccess( _anchor, _a, _text, _data )
{
	// Set the AJAX link state to finished ( to stop the repeating dots )
	_a.state = ANCHOR_STATE_FINISHED;

	// Let the user know the item was stored...
	_a.textContent = _text;
	_a.__loading_img.remove( );

	// Remove the original anchor debug text ( the asterisk )
	_anchor.textContent = " DEBUG ";
	_anchor.href = _data;
}


//
//
//
function __HandleScrollingIntoView( _anchor )
{
	//_anchor.scrollIntoView( );
}


//
// - Helper
//
function __HandleOnClick( _anchor, _a, _text )
{
	// Set the anchor state as waiting / processing ( so the dots will repeat )
	_a.state = ANCHOR_STATE_WAITING;

	// Append the Working / Processing / Fetching Data Language text to the link text..
	_a.textContent += _text;
	_a.__loading_img = __CreateLoadingIMG( _a );

	// Add the ... / .. / . / .. / ... process..
	__UpdateTextDotDotDot( _a, _a.textContent );

	// See if we can manipulate the parent div - this is so we can move it to saved list, or remove it...
	_anchor.parentNode.background = "#FF0000!";
	_anchor.parentNode.parentNode.background = "#00FF00!";
	_anchor.parentNode.parentNode.parentNode.background = "#0000FF!";

	// Prevent the browser from following the link so it happens in the background...
	return Acecool.util.StopEvent( _event );
}


//
// Helper -
//
function __HandleAddingOnClickFunctionality( _anchor, _text, _text_pending, _text_success, _text_fail, _text_search_success )
{
	var _url = _anchor.href;

	// Things that happen to ALL links we monitor..
	var _a = __HandleLink( _anchor, _text );

	// Edit the OnClick method for this link..
	//_anchor.attr( "onclick", function( _event )
	//$( _anchor ).click( function( _event )
	$( _a ).on( "click", function( _event )
	{

		// Make sure we don't go to the top...
		__HandleScrollingIntoView( _a );

		// OnSuccess / OnFail
		Acecool.http.Fetch( _url, function( _data ) {
			// var _result = _data.search( L_CART_ITEM_SAVE_SUCCESS );
			//var _result = _data.search( _text_search_success );
			//if ( _result > -1 )

			// If the "success" data exists on the page, then we may have been successful...
			if ( _data.IndexOfExists( _text_search_success ) )
				__HandleOnSuccess( _anchor, _a, _text + _text_success, _data );
			else
				// Alternate between <Text> [ Error ] and <Text> [ <FULL_ERROR> ] - end on generic Error...
				__UpdateAnchorText( _a, _text + _text_fail, _text + L_ERROR, 0 );
		}, function( _data ) {
			// Alternate between <Text> [ Error ] and <Text> [ <FULL_ERROR> ] - end on generic Error...
			__UpdateAnchorText( _a, _text + L_ERROR_404, _text + L_ERROR, 0 );
		} );

		//
		return __HandleOnClick( _anchor, _a, _text_pending );
	} );

	return _a;
}


//
// MAIN Helpers - //L_SUCCESS_UPDATE ); //
//
function __HandleSaveLink( _anchor ) { __HandleAddingOnClickFunctionality( _anchor, L_BUTTON_SAVE, L_SAVING, L_SAVED, L_ERROR_SAVING, L_SUCCESS_SAVE ); }
function __HandleRemoveLink( _anchor ) { __HandleAddingOnClickFunctionality( _anchor, L_BUTTON_REMOVE, L_REMOVING, L_REMOVED, L_ERROR_REMOVING, L_SUCCESS_REMOVE ); }
function __HandleAddLink( _anchor ) { __HandleAddingOnClickFunctionality( _anchor, L_BUTTON_ADD, L_ADDING, L_ADDED, L_ERROR_ADDING, L_SUCCESS_ADD ); }



//
// Helper - Called immediately as this script is executed on the page..
//
AddFeature( TAG_EBAY, TASK_ON_PRELOAD, function( _data, _cfg )
{
	if ( ACECOOL_CFG_USING_JQUERY )
	{
		// Process Site Specific Features for each DIV element
		var _desc_ext = document.getElementById( "snippetdesc" );

		//
		__Log( "eBay snippetdesc ................", true );

		// If it exists...
		if ( _desc_ext )
		{
			__Log( "eBay snippetdesc anchor exists..." );
			$( _desc_ext ).onclick( function( _event )
			{
				//
				__Log( "eBay modifying snippetdesc onclick function...", true );

				//
				Acecool.util.StopEvent( _event );

				//
				Acecool.http.Fetch( _desc_ext.href, function( _data )
				{
					__Log( "Ebay SnippetDesc added contents after it...", true );
					var _div = Acecool.html.CreateElement( 'div' );
					_div.innerHTML = _data;
					// _div.insertAfter( _desc_ext );
				} );
			} );
		}
	}
} );


//
//
//
AddFeature( TAG_EBAY, TASK_PROCESS_ANCHORS, function( _anchor, _cfg )
{
	// If we've already processed page links in any authorized task for this site, then don't process them again...
	if ( HasCycledOnce( 'anchors' ) !== false )
		return;

	// Unique ID
	_cfg.anchor_id = _cfg.anchor_id + 1;
	_anchor.__unique_id = _cfg.anchor_id;

	var _pattern =    _cfg.cart_regex;

	// The text the user sees - Note: HTML is stripped with .textContent and leading / trailing whitespace is trimmed.
	var _title = _anchor.textContent.trim( ); // Full Data: .innerHTML; // Custom Library Function: .StripHTML( );

	// URL
	var _url = _anchor.href;

	// Look in the URL for /sc/<this>?...............
	var _results = _pattern.exec( _url );

	// If we have any type of results... ie the table was at least created...
	if ( _results )
	{
		// Grab the result, or return "no_match"
		var _result = ( _results && _results[ 1 ] ) ? _results[ 1 ] : "no_match";

		// Debugging
		if ( ACB_DEBUG_MODE )
			console.log( "[ Acecool - eBay AJAX Cart ] Matched '" + _results[ 1 ] + "' on URL: " + _url + " with text: " + _title );

		// For each link we want to control, pass-through to our handler function...
		switch( _result )
		{
			// Remove from cart ==	/sc/rfc
			case URL_MATCH_REMOVE: //( _url.IndexOfExists( URL_MATCH_PREFIX + URL_MATCH_REMOVE ) ) ? _url : false: // L_BUTTON_REMOVE:
				__HandleRemoveLink( _anchor );
				_a.textContent += " [" + URL_MATCH_REMOVE + "]";

				break;

			// Remove from Save for later list ==	/sc/rfsfl
			case URL_MATCH_REMOVE_SAVED: //( _url.IndexOfExists( URL_MATCH_PREFIX + URL_MATCH_REMOVE_SAVED ) ) ? _url : false: //L_BUTTON_ADD:
				__HandleRemoveLink( _anchor );
				_a.textContent += " [" + URL_MATCH_REMOVE_SAVED + "]";

				break;
			// Save for later ==	/sc/sfl
			case URL_MATCH_SAVE: //( _url.IndexOfExists( URL_MATCH_PREFIX + URL_MATCH_SAVE ) ) ? _url : false: //L_BUTTON_SAVE:
				__HandleSaveLink( _anchor );
				_a.textContent += " [" + URL_MATCH_SAVE + "]";

				break;

			// Save for later list > Add to cart == /sc/sflatc
			case URL_MATCH_ADD_SAVED: //( _url.IndexOfExists( URL_MATCH_PREFIX + URL_MATCH_ADD_SAVED ) ) ? _url : false: //L_BUTTON_ADD:
				__HandleAddLink( _anchor );
				_a.textContent += " [" + URL_MATCH_ADD_SAVED + "]";

				break;

			default:
		}
	}
} );


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//