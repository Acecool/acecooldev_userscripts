//
//  - Josh 'Acecool' Moser
//
// ==UserScript==
// @name			Acecool - ____ - AcecoolWeb_Framework
// @author			Acecool
// @namespace		Acecool
// @version			0.0.1
// @description		Replaces encoded-links with decoded direct-links on episode finder sites.
// @description		Automatically click the "continue" button, and attempt to skip the countdown if any, on video watching sites.
// @description		Remove ad panels on video watching sites.
// @match			http://*/*
// @match			https://*/*
// @require			http://code.jquery.com/jquery-latest.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecool_functions_lib.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecoolweb_framework.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_default.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_steam.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_ebay.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_video.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecoolweb_framework_runtime.js
// @run-at			document-start
// @grant			GM_xmlhttpRequest
// @grant			GM_getResourceURL
// @grant			GM_addStyle
// @xrequire			https://gist.github.com/raw/2620135/checkForBadJavascripts.js
// ==/UserScript==


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// The fallback / default..
//

// Define the var used throughout ( I use _tag to make it easier to copy and paste for new additions, but I do keep TAG_X in the first AddFeature( ) call for my CodeMapper )
var _tag			= InitializeTag( 'default' );

// As SetupTagHosts returns the _tag enum value, I take this opportunity to set it - you can also define hosts to use, but as this is the default it acts as a fallthrough so everything runs through this...
const TAG_DEFAULT	= SetupTagHosts( _tag );

// Define configuration key / values for this tag...
// SetupTagConfig( _tag, 'key', 'value', 'key2', 'value2' );

// Setup Tasks: Preload, OnReady, OnLoad, OnFrameLoad, SetupObserver
SetupTasks( _tag, true, true, true, true, true );

// Setup Processes: Anchors, Divs, Frames, Observer
SetupProcesses( _tag, true, true, true, true );


//
// Add the Default Task for the Default Tag... This acts as the bottom of the pass-through for calls
//	ie if a site doesn't have a specific task function and it is set to true, like the above Task and Process map, then it falls-through to this function...
//
// Additionally, this acts as the primary / starting point of the pass-through as it would be a waste of space to define a TASK_DEFAULT for each and every site you add which performs the same task of routing
//	tasks to their own functions. This already fills that role; if you wanted to define a TASK_DEFAULT for your sites, so you could add the functionality all in a single function, you could do that which is
//	why I've left the switch commented out at the bottom - in the commented out state it shows how to route each call to their own function using TASK_ON_PRELOAD, or default: which does the same thing but shorter
//	and without the debugging comments..
//
// Note: Eventually, I will change the arguments for each task and more features for the entire system so be prepared for changes! This system was designed so you can use this script as a script that loads on all
// 		 sites, and then have other scripts with the rules load for each specific site ( or added to this script, but better if it isn't so you can allow auto-updating without losing your additions ), or add all
//		 of the rules to a separate single script - again to allow auto-updating on this script without losing your work!
//
AddFeature( TAG_DEFAULT, TASK_DEFAULT, function( _task, _data )
{
	// Fetch the TAG_*
	var _tag = GetActiveTag( );

	// Fetch our config table...
	var _cfg	= GetActiveConfigTable( );

	// See if our helper function exists...
	var _func	= GetTaskFunc( _task );

	// Helper function to output debugging info based on the current task on PreLoad..
	LogDebugPreLoadInfo( _task );


	//
	// Note: TAG_DEFAULT has both to show template example... Dynamic vs hard-coded dynamic...
	// The reason TAG_DEFAULT keeps this is because sometimes it makes more sense to keep it all contained in this func ( for debugging, etc.. )
	//	and then convert, or because it is short enough to the point of creating additional functions isn't worth the overhead..
	// TASK_ON_PRELOAD isfunction line can be copied to each TASK_* to have the same functionality as above
	//

	// Call helper-functions based on task - or use the switch below...
	if ( isfunction( _func ) )
	{
		LogDebugFuncExists( _task, _data, _cfg );
		// console.log( ' >> Using TAG_DEFAULT to launch Callback for: ' + GetTaskName( _task ) + ' - Which Exists!' );
		_func( _data, _cfg );
	}
	else
	{
		LogDebugFuncNotExists( _task, _data, _cfg );
		console.log( ' >> Using TAG_DEFAULT to launch Callback for: ' + GetTaskName( _task ) + ' - Which Does NOT Exist!' );
	}

	/*
	// TASK_ Switch!
	switch( _task )
	{
		// This is called immediately
		case TASK_ON_PRELOAD: break;
			// if ( isfunction( _func ) ) { LogDebugFuncExists( _task, _data, _cfg ); _func( _data, _cfg ); } else { LogDebugFuncNotExists( _task, _data, _cfg ); } break;

		// This is called on Document Ready - similar to window load
		case TASK_ON_DOCUMENT_READY: break;

		// Called on Window Load
		case TASK_ON_LOAD: break;

		// Called on IFrame Load
		case TASK_ON_FRAME_LOAD: break;

		// Called for each link on the page
		case TASK_PROCESS_ANCHORS: break;

		// Called for each div element on the page
		case TASK_PROCESS_DIVS: break;

		// Called for each iframe element on the page
		case TASK_PROCESS_FRAMES: break;

		// Called on Observer Mutation
		case TASK_ON_OBSERVER_MUTATION_CALLBACK: break;

		// Default case - if we're running a task which isn't listed above, this is the fallthrough...
		// The if is function line is the shortest you can go without the debugging calls of the above dynamic call...
		default:
			// if ( isfunction( _func ) ) { _func( _data, _cfg ); } break;
	} //*/
} );



//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// Localhost... Testing Grounds!
//

// Define the var used throughout ( I use _tag to make it easier to copy and paste for new additions, but I do keep TAG_X in the first AddFeature( ) call for my CodeMapper )
var _tag			= InitializeTag( 'localhost' );

// As SetupTagHosts returns the _tag enum value, I take this opportunity to set it - you can also define hosts to use, but as this is the default it acts as a fallthrough so everything runs through this...
const TAG_LOCALHOST	= SetupTagHosts( _tag, 'localhost', '127.0.0.1' );

// Define configuration key / values for this tag... You can use one per line, or set them up in sequence...
// SetupTagConfig( _tag, 'key', 'value', 'key2', 'value2' );

// Setup Tasks: Preload, OnReady, OnLoad, OnFrameLoad, SetupObserver
SetupTasks( _tag, true, false, false, false, false );

// Setup Processes: Anchors, Divs, Frames, Observer
SetupProcesses( _tag, false, false, false, false );


//
// Helper - Called immediately as this script is executed on the page..
//
AddFeature( TAG_LOCALHOST, TASK_ON_PRELOAD, function( _data, _cfg )
{
	// Read Cfg from Local and Global tables..
	// var _cfg_x GetCfg( _key, _default );

	// TODO: Work on openload system to get a DIRECT download link - when that is done
	alert( Acecool.util.GetHost( ) );
} );


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //


//
// Site Specific Templates
//


//
// Templates for all of the current tasks...
//
// Note: I will be adding alternate args per task soon...
//
// Note: Instead of directly accessing the table, you can now use a helper function: AddFeature( TAG_DEFAULT, TASK_DEFAULT, function( _task, _data ) { } );
//

// // Helper - Called immediately as this script is executed on the page..
// SiteSpecifcFeatures[ GetTaskFuncName( _tag, TASK_ON_PRELOAD ) ] = function( _data, _cfg ) { }

// // Helper - Called when the document has reached the ready-state..
// SiteSpecifcFeatures[ GetTaskFuncName( _tag, TASK_ON_DOCUMENT_READY ) ] = function( _data, _cfg ) { }

// // Helper - Called when the window has reached the on-loaded-state..
// SiteSpecifcFeatures[ GetTaskFuncName( _tag, TASK_ON_LOAD ) ] = function( _data, _cfg ) { }

// // Helper - Called for each IFrame OnLoaded / OnReady...
// SiteSpecifcFeatures[ GetTaskFuncName( _tag, TASK_ON_FRAME_LOAD ) ] = function( _data, _cfg ) { }

// // Process - Called for each A / Anchor element on the page.. For each non-process-task, process tasks are executed..
// SiteSpecifcFeatures[ GetTaskFuncName( _tag, TASK_PROCESS_ANCHORS ) ] = function( _anchor, _cfg ) { }

// // Process - Called for each DIV element on the page.. For each non-process-task, process tasks are executed..
// SiteSpecifcFeatures[ GetTaskFuncName( _tag, TASK_PROCESS_DIVS ) ] = function( _div, _cfg ) { }

// // Process - Processes each IFrame element on the page.. For each non-process-task, process tasks are executed..
// SiteSpecifcFeatures[ GetTaskFuncName( _tag, TASK_PROCESS_FRAMES ) ] = function( _frame, _cfg ) { }

// // Helper - Observer calls this function on Mutation
// SiteSpecifcFeatures[ GetTaskFuncName( _tag, TASK_ON_OBSERVER_MUTATION_CALLBACK ) ] = function( _data, _cfg, _mutations ) { }

// // Helper - Observer calls this function on Mutation
// SiteSpecifcFeatures[ GetTaskFuncName( _tag, TASK_SETUP_OBSERVER ) ] = function( _data, _cfg, _observer ) { }


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// Commented out...
//

/*


//
// Site Specific Template
//
// Note: I will be adding alternate args per task soon...
//
// Note: Instead of directly accessing the table, you can now use a helper function: AddFeature( TAG_DEFAULT, TASK_DEFAULT, function( _task, _data ) { } );
//

// Define the var used throughout ( I use _tag to make it easier to copy and paste for new additions, but I do keep TAG_X in the first AddFeature( ) call for my CodeMapper )
var _tag			= InitializeTag( 'my_friendly_tag_to_id_the_site' );

// As SetupTagHosts returns the _tag enum value, I take this opportunity to set it - you can also define hosts to use, but as this is the default it acts as a fallthrough so everything runs through this...
const TAG_EXAMPLE	= SetupTagHosts( _tag, 'example.com', 'example.net', 'example.org', 'example.gov' );

// Define configuration key / values for this tag... Note: Try uncommenting the SetTagConfig on the right and removing the call on the left... A different alert message will be displayed...
SetupTagConfig( _tag, 'show_alert', true ); // SetupTagConfig( _tag, 'show_alert', true, 'alert_text', 'Displaying alert on TAG_EXAMPLE website!' );

// Setup Tasks: Preload, OnReady, OnLoad, OnFrameLoad, SetupObserver
SetupTasks( _tag, true, true, true, true, true );

// Setup Processes: Anchors, Divs, Frames, Observer
SetupProcesses( _tag, true, true, true, true );


//
// This TASK_DEFAULT is the routing function - if set, if blank, then nothing will be called on this site... This example shows how short it can be if you want to reroute functions but not defining it does the same thing...
//	Another reason to define this is if you want to run all, or most, of the code from within this single function...
//
// Note: It is recommended NOT to set this - by not defining this feature, the TAG_DEFAULT fallback function will run which will automatically call the appropriate function...
//
AddFeature( TAG_EXAMPLE, TASK_DEFAULT, function( _task, _data )
{
	// Get the appropriate function for the current task
	var _func	= GetTaskFunc( _task );

	// If that function is set, call it...
	if ( isfunction( _func ) )
		_func( _data, _cfg );
} );


//
// Helper - Called immediately as this script is executed on the page..
//
// Note: The reason why I don't re-use TAG_* for each successive feature added is to make it more copy / paste friendly, and because my XCodeMapper extension for CodeMap automatically fills out that information
//
AddFeatures( _tag, TASK_ON_PRELOAD, function( _data, _cfg ) {
	// If we have show_alert set in our configuration, show an alert...
	if ( _cfg.show_alert )
		// If we have alert_text defined, use that text...
		if ( _cfg.alert_text )
			alert( _cfg.alert_Text );
		else
			alert( 'Displaying alert on TAG_EXAMPLE website with hostname: ' + Acecool.util.GetHost( ) + '!' );
} );


//
// Helper - Called when the document has reached the ready-state..
//
AddFeatures( _tag, TASK_ON_DOCUMENT_READY, function( _data, _cfg ) {

} );


//
// Helper - Called when the window has reached the on-loaded-state..
//
AddFeatures( _tag, TASK_ON_LOAD, function( _data, _cfg ) {

} );


//
// Helper - Called for each IFrame OnLoaded / OnReady...
//
AddFeatures( _tag, TASK_ON_FRAME_LOAD, function( _data, _cfg ) {

} );


//
// Process - Called for each A / Anchor element on the page.. For each non-process-task, process tasks are executed..
//
AddFeatures( _tag, TASK_PROCESS_ANCHORS, function( _anchor, _cfg ) {

} );


//
// Process - Called for each DIV element on the page.. For each non-process-task, process tasks are executed..
//
AddFeatures( _tag, TASK_PROCESS_DIVS, function( _div, _cfg ) {

} );


//
// Process - Processes each IFrame element on the page.. For each non-process-task, process tasks are executed..
//
AddFeatures( _tag, TASK_PROCESS_FRAMES, function( _frame, _cfg ) {

} );


//
// Helper - Observer calls this function on Mutation
//
AddFeatures( _tag, TASK_ON_OBSERVER_MUTATION_CALLBACK, function( _data, _cfg, _mutations ) {

} );


//
// Helper - Observer calls this function on Mutation
//
AddFeatures( _tag, TASK_SETUP_OBSERVER, function( _data, _cfg, _observer ) {

} );


//
// Commented out...
//
//*/




//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//