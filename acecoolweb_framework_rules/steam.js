//
//  - Josh 'Acecool' Moser
//
// ==UserScript==
// @name			Acecool - ____ - AcecoolWeb_Framework
// @author			Acecool
// @namespace		Acecool
// @version			0.0.1
// @description		Replaces encoded-links with decoded direct-links on episode finder sites.
// @description		Automatically click the "continue" button, and attempt to skip the countdown if any, on video watching sites.
// @description		Remove ad panels on video watching sites.
// @match			http://*/*
// @match			https://*/*
// @require			http://code.jquery.com/jquery-latest.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecool_functions_lib.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecoolweb_framework.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_default.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_steam.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_ebay.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_video.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecoolweb_framework_runtime.js
// @run-at			document-start
// @grant			GM_xmlhttpRequest
// @grant			GM_getResourceURL
// @grant			GM_addStyle
// @xrequire			https://gist.github.com/raw/2620135/checkForBadJavascripts.js
// ==/UserScript==


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// VidLox - Now properly gets rid of the adblock message and shows the file...
//

// Define the var used throughout ( I use _tag to make it easier to copy and paste for new additions, but I do keep TAG_X in the first AddFeature( ) call for my CodeMapper )
var _tag				= InitializeTag( 'steam_store' );

// As SetupTagHosts returns the _tag enum value, I take this opportunity to set it - you can also define hosts to use, but as this is the default it acts as a fallthrough so everything runs through this...
const TAG_STEAM_STORE	= SetupTagHosts( _tag, 'store.steampowered.com' );

// Define configuration key / values for this tag... You can use one per line, or set them up in sequence...
// SetupTagConfig( _tag, 'SteamKeyAccountLicensesURL', 'https://store.steampowered.com/account/licenses/' );

// Setup Tasks: Preload, OnReady, OnLoad, OnFrameLoad, SetupObserver
SetupTasks( _tag, false, false, true, false, false );

// Setup Processes: Anchors, Divs, Frames, Observer
SetupProcesses( _tag, false, false, false, false );


//
// Helper - ...
//
AddFeature( TAG_STEAM_STORE, TASK_ON_LOAD, function( _iframe, _cfg )
{
	// Read Cfg from Local and Global tables..
	// var _cfg_x GetCfg( _key, _default );

	// If we're not on the Purchases page, don't do anything...
	if ( true == false )
		return;

	// Load config...
	var _cfg_steam_url	= GetCfg( 'SteamKeyRedeemURL', '' );

} );


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// VidLox - Now properly gets rid of the adblock message and shows the file...
//

// Define the var used throughout ( I use _tag to make it easier to copy and paste for new additions, but I do keep TAG_X in the first AddFeature( ) call for my CodeMapper )
var _tag				= InitializeTag( 'bundle_cubic' );

// As SetupTagHosts returns the _tag enum value, I take this opportunity to set it - you can also define hosts to use, but as this is the default it acts as a fallthrough so everything runs through this...
const TAG_BUNDLE_CUBIC	= SetupTagHosts( _tag, 'cubicbundle.com' );

// Define configuration key / values for this tag... You can use one per line, or set them up in sequence...
// SetupTagConfig( _tag, 'SteamAPIKey', 'AC5F21543990B290C5C7A398B16A2F4B', 'FormatAPI', 'json', 'AvailableFormats', 'JSON, VDF, CSV, XML', 'SteamProfileID', 'Acecool', 'SteamID32', 'STEAM_0:1:4173055', 'SteamID64', '76561197968611839', 'SteamID3', '[U:1:8346111]', 'SteamKeyRedeemURL', 'https://store.steampowered.com/account/registerkey?key=' );

// Setup Tasks: Preload, OnReady, OnLoad, OnFrameLoad, SetupObserver
SetupTasks( _tag, false, false, true, false, false );

// Setup Processes: Anchors, Divs, Frames, Observer
SetupProcesses( _tag, false, false, false, false );


//
// Helper - ...
//
AddFeature( TAG_BUNDLE_CUBIC, TASK_ON_LOAD, function( _iframe, _cfg )
{
	// If we're not on the Purchases page, don't do anything...
	// if ( true == false )
	// 	return;

	// Load config...
	// var _cfg_steam_url	= _cfg.SteamKeyRedeemURL;
	// var _cfg_steam_api	= _cfg.SteamAPIKey;

	// Read Cfg from Local and Global tables..
	// var _cfg_steam_url = GetCfg( 'SteamKeyRedeemURL', '' );
	// var _cfg_steam_api = GetCfg( 'SteamAPIKey', '' );

	// var _steamid32, _steamid64, _steamid3 = '';
	// var _steamid32_a, _steamid32_b, _steamid32_c = 'x';
	// var _steamid64_a, _steamid64_b, _steamid64_c = 'x';
	// var _steamid3U_a, _steamid3U_b, _steamid3U_c = 'x';

	// Translate our SteamID to all 3...
	// [ _steamid32_a, _steamid64_a, _steamid3U_a ] = Acecool.SteamAPI.TranslateSteamID( GetCfg( 'SteamID32', '' ) );
	// console.log( ' >>> Converting SteamID 32 to All 3: ' + _steamid32_a + ' / ' + _steamid64_a + ' / ' + _steamid3U_a );

	// Debugging..
	// [ _steamid32_b, _steamid64_b, _steamid3U_b ] = Acecool.SteamAPI.TranslateSteamID( GetCfg( 'SteamID64', '' ) );
	// console.log( ' >>> Converting SteamID 64 to All 3: ' + _steamid32_b + ' / ' + _steamid64_b + ' / ' + _steamid3U_b );

	// Debugging..
	// [ _steamid32_c, _steamid64_c, _steamid3U_c ] = Acecool.SteamAPI.TranslateSteamID( GetCfg( 'SteamID3', '' ) );
	// console.log( ' >>> Converting SteamID 3/U to All 3: ' + _steamid32_c + ' / ' + _steamid64_c + ' / ' + _steamid3U_c );

	// [ _steamid32_c, _steamid64_c, _steamid3U_c ] = Acecool.SteamAPI.TranslateSteamID( _steamid64_c );
	// console.log( ' >>> Converting SteamID 3/U to All 3: ' + _steamid32_c + ' / ' + _steamid64_c + ' / ' + _steamid3U_c );

	var _api_url = GetCfg( 'SteamAPIGetOwnedGamesURL', '' ) + GetCfg( 'SteamAPIKey', '' ) + '&steamid=' + GetCfg( 'SteamID64', '' ) + '&format=' + GetCfg( 'FormatAPI', 'json' );
	Acecool.http.Fetch( _api_url, function( _data )
	{
		console.log( ' >> Steam GetOwnedGames Success!' );
		console.log( _data );
	} );

	// Grab the tables on the page..
	var _tables = document.getElementsByTagName( 'table' );

	// Debugging
	// console.log( ' >> CubicBundle >> Tables on page: ' + _tables.length );

	// Run through every table on the site...
	for ( var _i = 0; _i < _tables.length; _i++ )
	{
		// Helper Var
		var _table = _tables[ _i ];

		// Track Row Count
		var _r = 0;

		// Debugging
		// console.log( ' >> CubicBundle >> Rows in Tables on page: ' + _table.rows.length );

		// Process each row in the table
		while( _row = _table.rows[ _r++ ] )
		{
			// Track Cell Count
			var _c = 0;

			// Track Previous Data - Default is nothing...
			var _previous_data = '';

			// Debugging
			// console.log( ' >> CubicBundle >> Cells in Current Row: ' + _row.cells.length );

			// Process each cell in the row
			while( _cell = _row.cells[ _c++ ] )
			{
				// Grab the data from the cell...
				var _data = _cell.innerHTML.trim( );

				// Debugging
				// console.log( ' >> CubicBundle >> Data in Current Cell: ' + _data );

				// If we're on Steam, that means the game title is the previous cell - query to see if we own it or not, if yes mark RED, if no mark GREEN!
				if ( _data == 'Steam' )
				{
					// _cell.innerHTML = '[ S32 ] ' + _steamid32_a + ' / ' + _steamid64_a + ' / ' + _steamid3U_a;
					// _cell.innerHTML += '[ S64 ] ' + _steamid32_b + ' / ' + _steamid64_b + ' / ' + _steamid3U_b;
					// _cell.innerHTML += '[ S3U ] ' + _steamid32_c + ' / ' + _steamid64_c + ' / ' + _steamid3U_c;

				}

				// If the previous data is Steam, then we're on the Key Cell meaning we can add a Redeem Key link!
				switch( _previous_data )
				{
					// For Steam Store, add the Steam Redeem Key Link... U-Play, Origin, etc.. get other linkss...
					case 'Steam':
						// Modify the cell to be 'Redeem Key :: <KEY>'
						_cell.innerHTML = '<a href="' + GetCfg( 'SteamKeyRedeemURL', '' ) + _data + '">Redeem Key</a> :: ' + _data;

						break;

					// Nothing to do on default case..
					default:
				}


				// Update the previous data for the next cell...
				_previous_data = _data;
			}
		}
	}
} );


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//