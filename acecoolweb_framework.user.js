// ==UserScript==
// @name			Acecool - ____ - AcecoolWeb_Framework
// @author			Acecool
// @namespace		Acecool
// @version			0.0.1
// @description		Replaces encoded-links with decoded direct-links on episode finder sites.
// @description		Automatically click the "continue" button, and attempt to skip the countdown if any, on video watching sites.
// @description		Remove ad panels on video watching sites.
// @match			http://*/*
// @match			https://*/*
// @require			http://code.jquery.com/jquery-latest.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecool_functions_lib.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecoolweb_framework.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework_rules/default.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework_rules/ebay.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework_rules/projects.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework_rules/steam.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework_rules/video.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecoolweb_framework_runtime.js
// @run-at			document-start
// @grant			GM_xmlhttpRequest
// @grant			GM_getResourceURL
// @grant			GM_addStyle
// ==/UserScript==