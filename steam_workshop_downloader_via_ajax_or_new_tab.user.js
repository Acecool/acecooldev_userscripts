// ==UserScript==
// @name		Acecool - Steam Workshop Downloader
// @namespace	Acecool
// @version		0.1.0
// @description	Adds new buttons to Steam Workshop Items to Download or Download ( which reads the file into memory with progress bar; currently issue with saving from there.. ) via Tab ( which direct-links to the file ) and Collections to Download All via Ajax or Tab..
// @author		Josh 'Acecool' Moser
// @include		*steamcommunity.com/sharedfiles/filedetails/?id=*
// @include		*steamcommunity.com/workshop/filedetails/?id=*
// @require		http://code.jquery.com/jquery-latest.js
// @require		https://bitbucket.org/Acecool/acecooldev_userscripts/raw/master/libraries/acecool_functions_lib.js
// @grant		GM_xmlhttpRequest
// @license		ACL
// ==/UserScript==

//
// Steam Workshop Downloader - Josh 'Acecool' Moser
//
// Note		Set the update URL to: https://bitbucket.org/Acecool/acecooldev_userscripts/raw/master/steam_workshop/steam_workshop_downloader_via_ajax_or_new_tab.js
//
// Note		There is still a bug where ajax downloads aren't saved despite everything working properly ( except the save mechanism ).
//
// Note		When you download via Tabs the file may or may not have an extension... If it doesn't have 2 then add .gma.zip or .gma.gma to the end because you will need to extract it 
//			using 7zip before the gma file will be accessible to you so you can extract it and the file that is extracted will use the updated filename after you rename it ( odd )..
//


//
// For development purposes..
//
// @xrequire	file:C:/AcecoolGit/acecooldev_userscripts/libraries/acecool_functions_lib.js
// @xrequire	https://bitbucket.org/Acecool/acecooldev_userscripts/raw/master/libraries/acecool_functions_lib.js
// @xrequire	https://raw.githubusercontent.com/eligrey/Blob.js/master/Blob.js
// @xrequire	http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js
// @xrequire	https://raw.githubusercontent.com/eligrey/FileSaver.js/master/FileSaver.js


//
// ENUMeration and CONSTantS
//

// Download Methods...
var STEAM_WORKSHOP_DOWNLOAD_METHOD_AJAX = 0;
var STEAM_WORKSHOP_DOWNLOAD_METHOD_TABS = 1;

// URL used to retrieve data about an individual workshop addon
var WS_DETAILS_ADDON_URL = "http://api.steampowered.com/ISteamRemoteStorage/GetPublishedFileDetails/v0001/";

// URL used to retrieve data about all addons ( WSIDs etc.. ) in a collection...
var WS_DETAILS_COLLECTION_URL = "http://api.steampowered.com/ISteamRemoteStorage/GetCollectionDetails/v0001/";

// The workshop id of the current page ( for addon or collection )...
var WSID_PATTERN = new RegExp( "[0-9]{2,15}" );
var WSID = WSID_PATTERN.exec( document.URL );

//
var STEAM_IN_COLLECTION = steam_in_collection( );


//
// CONFIG
//

// If we want to view console log / output or save them to a file...
var STEAM_DEBUGGING = !false;

// Download Method... If Ajax it'll show a progress bar on screen, download then save to your downloads dir... If TABS method, it'll open a new tab for every addon...
// NOTE: If using TABS method, file-name isn't controlled by the script.. It is controlled by the site...
var STEAM_WORKSHOP_DOWNLOAD_METHOD = STEAM_WORKSHOP_DOWNLOAD_METHOD_AJAX;


//
// Functions / Code Begins Below....
//


//
// Steam website is built in a way that you can interchange the url between /sharedfiles/ and /workshop/ and it won't mean you're in a collection or not. So I am determining
// whether or not we're looking at a collection or a regular workshop item by reading the Breadcrumbs and stripping all HTML out so only plain-text is left, then I search for
// the exact word "Collections".. If it exists, we're in a collection. If it doesn't, we're in a normal addon...
// This seems to be the most solid way to determine Addon vs Collection unless there is something else that I've overlooked ( Did this quickly after noticing the issue )...
//
function steam_in_collection( )
{
	// Grab the raw breadcrumb html... // Strip all HTML tags ( leaving the innards ) // Then, search for "Collections" and return true if we have a valid index...
	return document.getElementsByClassName( "breadcrumbs" )[ 0 ].innerHTML.StripHTML( ).IndexOfExists( "Collections" );
}


//
// Save the file...
//
function save_workshop_file( _filename, _content, _filesize, _addon_created, _addon_updated, _type )
{
	//
	console.log( "Saving " + _filename + " to your Downloads Directory! Expected File-Size: " + Math.round( _filesize / 1024 ) + "KB or RAW-Bytes: " + _filesize );
	console.log( "Character count of " + _filename + " is " + _content.length );

	//var _datatype = ( !_type ) ? 'application/octet-stream' : _type;
    //var _datatype = ( !_type ) ? 'application/download' : _type;
    var _datatype = 'application/download';
	var _data = new Blob( [_content], { 'type':_datatype} );
	var _anchor = document.createElement( 'a' );
	_anchor.href = window.URL.createObjectURL( _content );
	_anchor.download = _filename;
	_anchor.click( );
}


//
// Begin Download Stream
//
function download_workshop_addon_stream( _wsid, _url, _addon_url, _addon_photo, _addon_filename, _addon_title, _addon_clean_filename, _addon_created, _addon_updated )
{
	// Grab the element on the page where we want to insert the download information...
	var _element = document.getElementById( "ItemControls" );

	// A simple Line Break to add...
	var _br = document.createElement( 'hr' );
	_br.setAttribute( 'height', '1px' );

	// The download progress bar...
	var _progress = document.createElement( 'progress' );
	_progress.setAttribute( 'id', 'progress_' + _wsid );
	_progress.setAttribute( 'value', '0' );

	// Create an img tag to display the addon preview-image next to the download info...
	var _img = document.createElement( 'img' );
	_img.setAttribute( 'id', 'progress_img_' + _wsid );
	_img.setAttribute( 'width', 64 );
	_img.setAttribute( 'src', _addon_photo );
	_img.setAttribute( 'alt', 'Addon: ' + _addon_title + ' will be saved as: ' + _addon_clean_filename );

	// Create a span tag to house information to display to the client in terms of how far along the download is...
	var _info = document.createElement( 'span' );
	_info.setAttribute( 'id', 'progress_info_' + _wsid );
	//_info.setAttribute( 'class', 'workshopItemDescription' );
	_info.innerHTML = 'Download Data!';

	// Add the elements to where we want them to appear on the page...
	_element.parentNode.appendChild( _br, _element );
	_element.parentNode.appendChild( _img, _element );
	_element.parentNode.appendChild( _progress, _element );
	_element.parentNode.appendChild( _info, _element );
	//_element.parentNode.appendChild( _br, _element );

	//
	var _addon_filesize = 0;


	//
	// Request the data and add the OnClick functionality to our DownloadBtn for Downloading ALL collection items...
	//
	GM_xmlhttpRequest( {
		method: "GET",
		//method: "POST",
		url: _addon_url,
		ignoreCache: true,
		redirectionLimit: 0,

		//
		// Header data to send...
		//
		headers:
		{
			//'Content-Type': 'application/x-www-form-urlencoded'
			// 'Content-Type': 'application/octet-stream'
            //"Content-Disposition": "attachment; filename=" + _addon_filename,
			"Content-Disposition": "inline; filename*=UTF-8''" + _addon_filename,
			"Content-Type": "application/octet-stream",
			//"Content-Type": "application/download",
		},

		//
		// Called when the Ready State has Changed... ( 1 = Init, 2 = Unknown, 3 = Progress Callback, 4 = Completed )
		//
		onreadystatechange: function( _response )
		{
			if ( STEAM_DEBUGGING )
				console.log( 'Ready State: ' + _response.readyState );
		},


		//
		// Called when data-state has changed.. ie progress has been made downloading the addon.. ( Same as ReadyStateChange ID 3 )
		//
		onprogress: function( _event )
		{
			if ( _event.lengthComputable )
			{
				_progress.max = _event.total;
				_progress.value = _event.loaded;
				_addon_filesize = _event.total;
				_info.innerHTML = 'Downloaded <b>' + Math.round( _event.loaded / 1024 ) + '</b>KB / <b>' + Math.round( _event.total / 1024 ) + '</b>KB of Addon <b>' + _addon_title + '</b> to be saved as: <b>' + _addon_clean_filename + '</b>!';

				if ( STEAM_DEBUGGING )
					console.log( 'Computing: ' + _event.loaded + ' / ' + _event.total );
			}
			else
			{
				// Unable to compute progress information since the total size is unknown
				if ( STEAM_DEBUGGING )
					console.log( 'Error computing file / data for Addon: ' + _addon_title );
			}
			},


		//
		// On Completed Callback... ( Same as ReadyStateChange ID 4 )
		//
		onload: function( _response )
		{
				var _log = "An error occurred."
					//+ "\nresponseText: " + _response.responseText
					+ "\nreadyState: " + _response.readyState
					+ "\nresponseHeaders: " + _response.responseHeaders
					+ "\nstatus: " + _response.status
					+ "\nstatusText: " + _response.statusText
					+ "\nfinalUrl: " + _response.finalUrl;
				console.log( _log );

			// Notify the client that the file has downloaded successfully...
			_info.innerHTML = '<font color="#00FF00"><b>' + _addon_title + '</b> has successfully downloaded and is saved as: <b>' + _addon_clean_filename + '</b> File-Size: <b>' + Math.round( _addon_filesize / 1024 ) + '</b>KB!</font>';

			// Delay removing elements for a few seconds...
			setTimeout( function( )
			{
				_br.remove( );
				_img.remove( );
				_progress.remove( );
				_info.remove( );
			}, 10000 );

			// Save the file..
			// ( _wsid, _url, _addon_url, _addon_photo, _addon_filename, _addon_title, _addon_clean_filename )
			save_workshop_file( _addon_clean_filename, _response.responseText, _addon_filesize, _addon_created, _addon_updated );
		},


		//
		// Error Callback
		//
		onerror: function( _error )
		{
			if ( STEAM_DEBUGGING )
				console.log( _error );
		}
	} );
}


//
// Download an invididual workshop addon
//
function download_workshop_addon( _wsid, _url, _override )
{
	GM_xmlhttpRequest( {
		method: "POST",
		url: _url,
		ignoreCache: true,
		redirectionLimit: 0,
		data: "itemcount=1&publishedfileids[0]=" + _wsid + "&format=json",


		//
		// Header data to send...
		//response.setHeader("Content-Disposition", "attachment; filename=file.zip");
		headers:
		{
			"Content-Type": "application/x-www-form-urlencoded",
            //"Content-Disposition": "attachment; filename=file.zip"
			//"Content-Type": "application/json; charset=UTF-8",
			//"Content-Encoding": "gzip"
		},


		//
		// On Completed Callback... ( Same as ReadyStateChange ID 4 )
		//
		onload: function( _response )
		{
			//
			if ( STEAM_DEBUGGING )
				console.log( _response.responseText ) ;

			var _data = jQuery.parseJSON( _response.responseText ).response.publishedfiledetails[ 0 ];

			// Addon Download Link
			var _addon_url = _data.file_url;

			// Addon Photo Link
			var _addon_photo = _data.preview_url;

			// Addon Workshop ID ( Should be the same as _wsid )
			var _addon_wsid = _data.publishedfileid;

			// Addon Title
			var _addon_title = _data.title;

			// Addon File Name
			var _addon_filename = _data.filename;

			// Date / Time
			var _addon_created = _data.time_created;
			var _addon_updated = _data.time_updated;

			// Gives us a new file-name generated from the file-name, workshop id and file-extension...
			var _addon_clean_filename = _addon_title.Clean( ) + "_" + _addon_wsid + "" + _addon_filename.FileEXT( ) + "" + _addon_filename.FileEXT( );

			// Depending on the configured method of downloading addons.. It'll either redirect for a tab to open up, or it'll stream it with a progress bar using ajax on the site...
			// Note: For single-addons we don't need to open a new tab so that is incorporated in the code below...
			if ( !_override && STEAM_WORKSHOP_DOWNLOAD_METHOD == STEAM_WORKSHOP_DOWNLOAD_METHOD_AJAX )
				download_workshop_addon_stream( _addon_wsid, _url, _addon_url, _addon_photo, _addon_filename, _addon_title, _addon_clean_filename, _addon_created, _addon_updated );
			else
				if ( STEAM_IN_COLLECTION )
					window.open( _addon_url, '_blank' );
				else
					window.location.href = _addon_url;
		},


		//
		// Error Callback
		//
		onerror: function( _error )
		{
			if ( STEAM_DEBUGGING )
				console.log( _error );
		}
	} );
}


//
// Adds the "Download" button to a workshop page
//
function add_download_button( _text, _override )
{
	var _element = document.getElementById( "AddToCollectionBtn" );
	var _DownloadBtnID = ( _override ) ? "DownloadBtnOverride" : "DownloadBtn";

	// Create a new "span" tag / element and set it up...
	var _button = document.createElement( 'span' );
	_button.setAttribute( 'class', 'general_btn share tooltip' );
	_button.innerHTML = '<span id="' + _DownloadBtnID + '"><span>' + _text + '</span></span>';

	// Append the element after the real subscribe button
	if ( _element.nextSibling )
		_element.parentNode.insertBefore( _button, _element.nextSibling );
	else
		_element.parentNode.appendChild( _button );
}


//
// Adds the Download functionality to our download button...
//
function add_download_onclick( _wsid, _url, _collection, _override )
{
	// Use different _GET params depending on whether we're looking at a collection or not...
	var _xml_data = ( _collection ) ? "collectioncount=1&publishedfileids[0]=" + _wsid + "&format=json" : "itemcount=1&publishedfileids[0]=" + _wsid + "&format=json";
	var _DownloadBtnID = ( _override ) ? "DownloadBtnOverride" : "DownloadBtn";

	//
	// Request the data and add the OnClick functionality to our DownloadBtn for Downloading ALL collection items...
	//
	GM_xmlhttpRequest( {
		method: "POST",
		url: _url,
		ignoreCache: true,
		redirectionLimit: 0,
		data: _xml_data,


		//
		// Header data to send...
		//
		headers:
		{
			"Content-Type": "application/x-www-form-urlencoded"
		},


		//
		// On Completed Callback... ( Same as ReadyStateChange ID 4 )
		//
		onload: function( _response )
		{
			//
			if ( STEAM_DEBUGGING )
				console.log( _response.responseText );

			// Another Collection Swap for the addon / collection data ... If we're using collection use "collectiondetails", otherwise use "publishedfiledetails"...
			var _data = jQuery.parseJSON( _response.responseText ).response[ ( ( _collection ) ? "collectiondetails" : "publishedfiledetails" ) ][ 0 ];

			// Download Button OnClick Method...
			$( "#" + _DownloadBtnID ).click( function( _event )
			{
				// Prevent the browser window from redirecting to the post / page in this query, so it happens in the background, unless we use tab download method.. for AJAX it is ok to prevent redirect, not for tabs...
				if ( STEAM_WORKSHOP_DOWNLOAD_METHOD == STEAM_WORKSHOP_DOWNLOAD_METHOD_AJAX )
					_event.preventDefault( );


				// If we're using a collection, process each addon.. Otherwise simply download the one we are looking at...
				if ( _collection )
				{
					// ForEach Addon in the Collection, call our Download Function...
					$.each( _data.children, function( i, _addon )
					{
						download_workshop_addon( _addon.publishedfileid, WS_DETAILS_ADDON_URL, _override );
					} );
				}
				else
				{
					// Redirect this window to the download link for basic download mechanism...
					// window.location.href = _addon_url;
					download_workshop_addon( _wsid, _url, _override );
				}
			} );
		},


		//
		// Error Callback
		//
		onerror: function( _error )
		{
			if ( STEAM_DEBUGGING )
				console.log( _error );
		}
	} );
}




//
// Init...
//

// This adds the "Download" or "Download All" button and the OnClick method along with querying the relevant data in order to know what to download and where...
if ( STEAM_IN_COLLECTION )
{
	// Welcome Message
	console.log( "[ Acecool ][ Steam Workshop Downloader ][ Collection Mode ]" );

	if ( STEAM_WORKSHOP_DOWNLOAD_METHOD == STEAM_WORKSHOP_DOWNLOAD_METHOD_AJAX )
	{
		// Add our Download Button
		add_download_button( "Download All via Tabs", true );

		// Same as above, but forces open in tabs if AJAX mode is enabled...
		add_download_onclick( WSID, WS_DETAILS_COLLECTION_URL, true, true );
	}

	// Add our Download Button
	add_download_button( "Download All" );

	// Add the OnClick method for our DownloadBtn and query the collection data so the OnClick will start downloading all files...
	add_download_onclick( WSID, WS_DETAILS_COLLECTION_URL, true, false );
}
else
{
	// Welcome Message
	console.log( "[ Acecool ][ Steam Workshop Downloader ][ Single Addon Mode ]" );

	if ( STEAM_WORKSHOP_DOWNLOAD_METHOD == STEAM_WORKSHOP_DOWNLOAD_METHOD_AJAX )
	{
		// Add our Download Button
		add_download_button( "Download via Tabs", true );

		// Same as above, but forces open in tabs if AJAX mode is enabled...
		add_download_onclick( WSID, WS_DETAILS_ADDON_URL, false, true );
	}

	// Add our Download Button for a Single Addon
	add_download_button( "Download" );

	// Add the OnClick method for our DownloadBtn and query the addon data so the OnClick will start downloading the addon file...
	add_download_onclick( WSID, WS_DETAILS_ADDON_URL );
}