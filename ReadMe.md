#
# Acecool's TamperMonkey -  GreaseMonkey - Other / Web Scripts...
#


##
## Installation!
##
- Click on AcecoolWeb_Framework.user.js to install it with TamperMonkey or your favorite site injector...
- Download the entire repo using this link:
- Extract to: C:\AcecoolGit\AcecoolDev_UserScripts
- Delete the ruleset files you do not want ( Delete the contents - or rename them and create new EMPTY JavaScript files with their original name to ensure there are no inclusion errors ).


##
## Notes
##
- In progress


##
## Rule Sets
##
- eBay
	- Currently we support a very basic eBay shopping cart - you can send the requests for Removing items from your cart, Removing from your cart to the saved-list, and re-adding from saved list. The DIVs are not updated - only the text.. this will change later... This was a proof of concept. Additionally, if you click the links too quickly, eBay will claim the successfully transferred ( eBay says it, not the script ) but it won't have happened... I need to look into how eBay handles carts - if they use cookies then I'll simply add a system to modify the cookies. If they use the web-system then I'll add a queue system to ensure the modifications take time to occur so they actually happen properly... The script properly detects errors - but for some reason eBay returns success note when there was an error...

- Steam Workshop
	- Support downloading ALL files in a collection at once using AJAX technology and live-progressbars of the download process. Due to limitations and security, downloading this way doesn't actually save the file - I am looking into a workaround... The request works, and downloads actually occur in the sense that you receive the data but the file isn't saved...
	- You can save files one at a time using workshop collections or workshop items - this saves the file.
	- Known Issues: Currently the name of the item saved is wonky and you have to add .gma to the first file and extract it in order to receive the proper GMA file which can be extracted and contains the data... You may also need to extract it to get the gma file in order to use it as an addon or for your server - I am working on this...

- Bitbucket
	- Modifies all Project Source links ( except for dif / history modes ) to point to src/master/ in order to link short-urls to your friends. Uses an observer so when BitBucket uses AJAX to reload source-container, the new links are modified too so the page doesn't need to be reloaded each time... It also includes BACK BUTTON SUPPORT so you still don't need to reload the page!

- Video Sites
	- Most video sites which host stolen content are malicious - I do not recommend using these sites. This rule-set is to remove malicious code, and bypass other basic protection blocking videos displaying when the uploader wants you to disable adblock ( even if it isn't installed - the error typically always appears on malicious sites unless you're using a vulnerable browser )... I add warnings to known malicious sites and try to remove the risks.

- Default
	- This provides examples of what you can do.



##
## Known Issues
##
- Unfortunately Extracting to C:\AcecoolGit\AcecoolDev_UserScripts isn't modifiable yet - I'll look into it later...



##
## Frequently Asked Questions
##
- Can I install the ruleset files and other files somewhere else?
- A: Not yet - I'll be looking into this soon...