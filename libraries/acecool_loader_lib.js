//
//  - Josh 'Acecool' Moser
//


//
// Check if jQuery was initialized and if not (CDN was down for example), then load jQuery from a local source.
// http://stackoverflow.com/questions/12332697/whats-the-best-way-to-load-jquery
//
// Help:
//			http://lab.abhinayrathore.com/jquery-standards/
//
// jQuery		(code.jquery.com)						<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
//	+ Plugins	http://plugins.jquery.com/tag/ajax/
// Microsoft	(ajax.microsoft.com)					<script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.8.0.min.js" type="text/javascript"></script>
// Google		(ajax.googleapis.com)					<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
//
if( typeof jQuery === 'undefined' )
{
    document.write( unescape( "%3Cscript src='yourlocalpath/jquery.1.x.min.js' type='text/javascript'%3E%3C/script%3E" ) );
}