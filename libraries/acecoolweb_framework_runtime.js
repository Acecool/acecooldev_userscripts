//
//  - Josh 'Acecool' Moser
//
// ==UserScript==
// @name			Acecool - ____ - AcecoolWeb_Framework
// @author			Acecool
// @namespace		Acecool
// @version			0.0.1
// @description		Replaces encoded-links with decoded direct-links on episode finder sites.
// @description		Automatically click the "continue" button, and attempt to skip the countdown if any, on video watching sites.
// @description		Remove ad panels on video watching sites.
// @match			http://*/*
// @match			https://*/*
// @require			http://code.jquery.com/jquery-latest.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecool_functions_lib.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecoolweb_framework.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_default.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_steam.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_ebay.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/acecoolweb_framework__ruleset_video.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecoolweb_framework_runtime.js
// @run-at			document-start
// @grant			GM_xmlhttpRequest
// @grant			GM_getResourceURL
// @grant			GM_addStyle
// @xrequire			https://gist.github.com/raw/2620135/checkForBadJavascripts.js
// ==/UserScript==


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// Monitor source-container div for changes and update links on change...
//
var _observer = new MutationObserver( function( _mutations )
{
	__main_ultimate_site_manager_script__( TASK_ON_OBSERVER_MUTATION_CALLBACK, _mutations );
} );

// RunTime.Get / SetObserver( )
AccessorFunc( RunTime, 'Observer', null );

// Set it so any scripts which need access to the observer can make use of it...
RunTime.SetObserver( _observer );


//
// Called as soon as this script has loaded
//
__main_ultimate_site_manager_script__( TASK_ON_PRELOAD );


//
// Setup calls to the primary runtime 'main' function at various events of a page loading life-cycle..
//
if ( ACECOOL_CFG_USING_JQUERY )
{
	//
	// Called when the page has finished loading
	//
	$( document ).ready( function( )
	{
		__main_ultimate_site_manager_script__( TASK_ON_DOCUMENT_READY );
	} );


	//
	// Called when the window has finished rendering or loading
	//
	$( window ).load( function( )
	{
		__main_ultimate_site_manager_script__( TASK_ON_LOAD );
	} );


	//
	// Replace Site Specific Features for ads, etc... when iframes load...
	//
	$( 'iframe' ).load( function( )
	{
	    __main_ultimate_site_manager_script__( TASK_ON_FRAME_LOAD );
	} );


	//
	// Replace Site Specific Features for ads, etc... when iframes load...
	//
	$( 'div' ).load( function( )
	{
	    __main_ultimate_site_manager_script__( TASK_ON_FRAME_LOAD );
	} );
}
else
{

}






















