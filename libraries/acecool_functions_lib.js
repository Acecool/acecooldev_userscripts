//
// Acecool™ Complete Functions Library - Josh 'Acecool' Moser
//


//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// Namespaces
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//

//
var Acecool			= Acecool || { };

// Internal stuff
// Acecool.__data		= Acecool.__data || { };
// Acecool.data		= Acecool.data || { };


//
Acecool.debug		= Acecool.debug || { };
Acecool.debugging	= Acecool.debugging || { };

//
Acecool.http		= Acecool.http || { };
Acecool.html		= Acecool.html || { };
Acecool.language	= Acecool.language || { };
Acecool.math		= Acecool.math || { };
Acecool.string		= Acecool.string || { };
Acecool.util		= Acecool.util || { };
Acecool.resources	= Acecool.resources || { };
Acecool.table		= Acecool.table || { };


//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// Config
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//

// Are we using JQuery or not? Note: The full JQuery exclusion statements havn't been coded yet
const ACECOOL_CFG_USING_JQUERY												= true;


//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// Configurable Options - Constants / Definitions / Enumeration / Vars / Maps
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//

// Enable or disable debug mode for the Acecool Name-Space...
const ACECOOL_DEBUG_MODE													= false;

// Mitigates what is debugged... This is so we can target debugging to a single script in the namespace...
var ACECOOL_DEBUG_FILTER													= '';


//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// Important Non-Configurable - Declarations / CONSTants / ENUMeration / ETC..
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//


//
// Time Declarations - in Seconds for the various times..
//

// One Second to a second..
const TIME_SECOND		= 1;

// 60 Seconds to a minute..
const TIME_MINUTE		= TIME_SECOND * 60;

// 3600 Seconds or 60 minutes to an hour..
const TIME_HOUR			= TIME_MINUTE * 60;

// 86400 Seconds or 1440 minutes or 24 hours in a day..
const TIME_DAY			= TIME_HOUR * 24;

// 172,800 seconds or 2,880 minutes or 48 hours or 2 days to a weekend..
const TIME_WEEK_END		= TIME_DAY * 2;

// 432,000 seconds or 10,080 minutes or 168 hours or 5 days to a work week..
const TIME_WORK_WEEK	= TIME_DAY * 5;

// 604,800 seconds or 7,200 minutes or 120 hours or 5 days to a full week..
const TIME_WEEK			= TIME_DAY * 7;

// 31,557,600 seconds or 525,960 minutes or 8,766 hours or 365.25 days in a year..
const TIME_YEAR			= TIME_DAY * 365.25;

// 2,629,800 seconds or 43,830 minutes or 730.5 hours or 30.4375 days in a month..
const TIME_MONTH		= TIME_YEAR / 12; // or TIME_DAY		* 30.4375;


//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// Global Functions
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//


//
// is<type> Helper-Functions..
//
function isfunction( _data ) { return typeof( _data ) == 'function'; }
function isstring( _data ) { return typeof( _data ) == 'string'; }
function isundefined( _data ) { return typeof( _data ) == 'undefined'; }
function isnull( _data ) { return typeof( _data ) == 'object'; }
function isbool( _data ) { return typeof( _data ) == 'boolean'; }
function isboolean( _data ) { return typeof( _data ) == 'boolean'; }
function isnumber( _data ) { return typeof( _data ) == 'number'; }
function issymbol( _data ) { return typeof( _data ) == 'symbol'; }
// function isset( _data ) { return ( isbool( _data ) || _data ); }


//
//
//
function IsNumberKey( _text, _event, _allow_decimals )
{
	// Char Code...
	var _char = ( _event.which ) ? _event.which : _event.keyCode;

	// If char is in range of numbers, or out of range as decimal if more than one decimal exists - return false
	if ( !( _char >= 48 && _char <= 57 || _allow_decimals && _char == 46 && !_text.value.HasDecimal( ) ) )
		return false;

	return true;
}


//
//
//
function ProcessUrlGetValues( )
{
	// Contains ?var1_value1&var2=value2&repeating=repeating
	var _search			= window.location.search;

	// Explode the string, excluding ?, using & as the delimiter...
	var _components		= _search.substr( 1 ).split( "&" );

	// Init $_GET...
	var $_GET			= { };

	// For each component, add it to our $_GET array...
	for ( var i = 0; i < _components.length; i++ )
	{
		// Explode our var1=value into an array...
		var _parts		= _components[ i ].split( "=" );

		// Now we have each individual key and value...
		var _key		= decodeURIComponent( _parts[ 0 ] );
		var _value		= decodeURIComponent( _parts[ 1 ] );

		// Assign it to the get value...
		$_GET[ _key ]	= _value;
	}

	// Return it...
	return $_GET;
}


//
// TODO: Remove the functions below...
//


//
// Cleans a string-name for file-name... Removes non-alphanumeric chars
//
function string_clean( _text )
{
	// Replaces all spaces and hyphens with an underscore.. Replaces all non alpa-numeric chars with nothing and converts the string to lowercase..
	return _text.replace( /[ -]/gi, '_' ).replace( /[^a-z0-9]/gi, '' ).toLowerCase( );
}


//
// Returns the file extension..
//
function string_file_ext( _name )
{
	var _pattern = /( ?:\.( [^.]+ ) )?$/;
	return _pattern.exec( _name )[ 1 ];
}


//
//
//
function string_strip_html( _text )
{
	var _temp = document.createElement( "DIV" );
	_temp.innerHTML = _text;
	var _data = _temp.textContent || tmp.innerText || "";
	return _data;
}


//
// Search using Needle, Haystack and return true if the word exists, false if it doesn't...
//
function string_boolean_search( _word, _text )
{
	return ( _text.indexOf( _word ) > 0 );
}


//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// Acecool - Document Functions
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//


//
// Helper - document.GetSource( ) - returns the current page source-code..
//
HTMLDocument.prototype.GetSource = function( )
{
	return this.getElementsByTagName( 'html' )[ 0 ].outerHTML;
};


//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// Object / Element Class Extension
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//


//
// Add a "Meta-Table" Function for Elements ( such as span, a, etc.. useful for ( _download = document.createElement( 'a' ); _download.insertAfter( _download_anchor ); )
//
Element.prototype.insertAfter = function( _element )
{
	_element.parentNode.insertBefore( this, _element.nextSibling );
};


//
//
//
Element.prototype.addBefore = function( _element )
{
	_element.parentNode.insertBefore( this, _element.prevSibling );
};


//
// Aliases
//
Element.prototype.addAfter = Element.prototype.insertAfter;
Element.prototype.AddAfter = Element.prototype.insertAfter;
Element.prototype.Afteradd = Element.prototype.insertAfter;
Element.prototype.AfterAdd = Element.prototype.insertAfter;

// Element.prototype.addBefore = Element.prototype.addBefore;
Element.prototype.AddBefore = Element.prototype.addBefore;
Element.prototype.Beforeadd = Element.prototype.addBefore;
Element.prototype.BeforeAdd = Element.prototype.addBefore;


//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// Acecool - Form Entry Data...
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//


//
// Add to form element such as input by adding ( req* optional, Use this or this.value ): required onkeypress="return form.IsNumericKey( this, event );"
//
HTMLFormElement.prototype.IsNumericKey = function( _element, _event, _allow_negative = true, _allow_decimals = true )
{
	// Char Code...
	var _char = ( _event.which ) ? _event.which : _event.keyCode;
	var _text = ( typeof( _element.value ) == "string" ) ? _element.value : ( ( typeof( _element ) == "string" ) ? _element : "" );

	// Boolean Statements
	var _bNumericRange	= ( _char >= 48 && _char <= 57 );
	var _bDecimal		= ( _allow_decimals && _char == 46 && !_text.HasDecimal( ) );
	var _bHyphen		= ( _allow_negative && _char == 45 && !_text.HasHyphen( ) );
	var _bPlus			= ( _allow_negative && _char == 43 && _text.HasHyphen( ) );

	// Debugging...
	//alert( 'Text: ' + _text + '; CharCode: ' + _char + '; HasHyphen: ' + _text.HasHyphen( ) + '; HasDecimal: ' + _text.HasDecimal( ) + ';' );
	//alert( _text.charCodeAt( 0 ) + ' == First Character Char-Code!' );

	// Add hyphen ( always 1st char ) if negative values are allowed.. Update _text variable too ( or use _element.value when checking for hyphen so extra returns aren't needed )...
	if ( _bHyphen )
		_element.value = '-' + _text;

	// Remove the hyphen when + is pressed if hyphen exists...
	if ( _bPlus )
		_element.value = _text.substring( 1 );

	// Boolean Statements - Refresher to include updated text...
	_bHyphen			= ( _bHyphen && !_element.value.HasHyphen( ) );

	// If char is in range of numbers, or out of range as decimal if more than one decimal exists - return false
	if ( !( _bNumericRange || _bDecimal || _bHyphen ) )
		return false;

	return true;
};


//
//
//
HTMLFormElement.prototype.SetNumericOnly = function( _element, _event, _allow_decimals )
{
	this.onkeypress = this.IsNumericKey( _element.value, _event, _allow_decimals );
};


//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// String Class Extension
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//


//
// Cleans a string-name for file-name... Removes non-alphanumeric chars
//
String.prototype.Clean = function( )
{
	// Replaces all spaces and hyphens with an underscore.. Replaces all non alpa-numeric chars with nothing and converts the string to lowercase..
	return this.replace( /[ -]/gi, '_' ).replace( /[^a-z0-9]/gi, '' ).toLowerCase( );
};


//
// Strips all HTML Elements from the string ( ie <xxxxxx>This isn't removed..</xxxxxx>Kept... ) with <>s removed too...
//
String.prototype.StripHTML = function( )
{
	var _temp = document.createElement( "DIV" );
	_temp.innerHTML = this;
	var _data = _temp.textContent || _temp.innerText || "";
	return _data;
};


//
// Returns the file extension..
//
String.prototype.FileEXT = function( )
{
	var _pattern = /(\.([^.]+))?$/;
	var _exec = _pattern.exec( this );
	return _exec[ 0 ];
};


//
// Searches string for word and returns true if it exists at least once...
// Search using Needle, Haystack and return true if the word exists, false if it doesn't...
//
String.prototype.IndexOfExists = function( _word, _from = 0 )
{
	return ( this.indexOf( _word, _from ) > -1 );
};

// Alias
String.prototype.Contains = String.prototype.IndexOfExists;


//
// Searches string for word and returns true if it exists at least once...
// Search using Needle, Haystack and return true if the word exists, false if it doesn't...
//
String.prototype.SearchResultsExist = function( _word )
{
	return ( this.search( _word ) > -1 );
};

// Alias
// String.prototype.Exists = String.prototype.SearchResultExists;
String.prototype.SearchExists = String.prototype.SearchResultExists;
// String.prototype.ExistsSearch = String.prototype.SearchResultExists;


//
// Searches string for word and returns true if it exists at least once...
//
String.prototype.CharCodeAtMatches = function( _pos, _code )
{
	return ( this.charCodeAt( _pos ) == _code );
};


//
// Searches string for word and returns true if it exists at least once...
//
String.prototype.HasHyphen = function( )
{
	// return this.CharCodeAtMatches( 0, 45 );
	return this.IndexOfExists( '-' );
};


//
// Searches string for word and returns true if it exists at least once...
//
String.prototype.HasDecimal = function( )
{
	return this.IndexOfExists( '\.' );
};


//
// Helper to return n characters of our string from the beginning..
//
String.prototype.StartChars = function( _count = 1 )
{
	return ( this.substring( 0, _count ) );
};


//
// Helper to return n characters of our string from the end..
//
String.prototype.EndChars = function( _count = 1 )
{
	return ( this.substring( this.length - _count, _count ) );
};


//
// Returns whether or not a string starts with the provided text
//
String.prototype.StartsWith = function( _needle )
{
	// Grab the needles length..
	var _len = _needle.length;

	// We can't search for something with 0 length..
	if ( _len == 0 )
		return false;

	// Does the haystack contain our needle at the beginning?
	return ( this.StartChars( _len ) === _needle );
};


//
// Returns whether or not a string ends with the provided text
//
String.prototype.EndsWith = function( _needle )
{
	// Grab the needles length..
	var _len = _needle.length;

	// We can't search for something with 0 length..
	if ( _len == 0 )
		return false;

	// Does the haystack contain our needle at the end?
	return ( this.EndChars( _len ) === _needle );
};


//
// Helper function used to SubString between a start point, and an end point...
//
String.prototype.SubSlice = function( _needle_start = '', _needle_end = '', _offset_start = 0, _offset_end = 0 )
{
	// Extract the snippet from the haystack...
	var _start = this.indexOf( _needle_start );
	var _end = this.indexOf( _needle_end, _start );

	// Now, compile all of the previous data to have cleaned extracted code...
	var _data = this.substring( _start + _needle_start.length + _offset_start, _end - _offset_end );

	// Return the extracted data...
	return _data;
}


//
// Helper - Evaluate / Execute Code extracted from haystack / this via SubString ( start + offset_start, end - offset_end ) - If debug is true, the code is returned instead of executed..
//
String.prototype.ExecuteScriptSlice = function( _needle_start = '', _needle_end = '', _offset_start = 0, _offset_end = 0, _debug = false )
{
	return Acecool.string.ExecuteScriptSlice( this, _needle_start, _needle_end, _offset_start, _offset_end, _debug );
}


//
// Helper - Converts String to number...
//
String.prototype.toNumber = function( )
{
	return Number( this );
}


//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// String Functions
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//




//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// Array Functions
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//


//
//
//
Array.prototype.insert = function( ..._varargs )
{
	return this.slice( 0, 0 ).concat( _varargs, this.slice( 0 ) );
}


//
//
//
Array.prototype.insertAt = function( _index, ..._varargs )
{
	return this.slice( 0, _index ).concat( _varargs, this.slice( _index ) );
}

//   function( _index )
// {
//     _index = Math.min( _index, this.length );
//     if ( arguments.length > 1 )
//     {
//         && this.splice.apply(this, [index, 0].concat([].pop.call(arguments)))
//         && this.insert.apply(this, arguments);
//     return this;
// };


// const insert = (arr, index, ...newItems) => [
//   // part of the array before the specified index
//   ...arr.slice(0, index),
//   // inserted items
//   ...newItems,
//   // part of the array after the specified index
//   ...arr.slice(index)
// ]

// const result = insert(items, 1, 10, 20)


//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// Array Functions
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//


//
// Helper - Evaluate / Execute Code extracted from Haystack - SubString / Slice the _haystack from using needle start as the search term, and the location to continue the search for needle end.. Offsets clean it up - start goes forward and end subtracts..
//
Acecool.string.ExecuteScriptSlice = function( _haystack = '', _needle_start = '', _needle_end = '', _offset_start = 0, _offset_end = 0, _debug = false )
{
	// Grab the data
	var _data = _haystack.SubSlice( _needle_start, _needle_end, _offset_start, _offset_end );

	// Execute the code..
	if ( _data )
		if ( !_debug )
			return new Function( _data )( );
		else
			return _data;

	// For errors sake..
	return false;
}


//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// Acecool.table..
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//


//
// Helper - Runs for-each on an array via callback...
//
Acecool.table.foreach = function( _tab, _callback )
{
	// for ( var _i = 0, _len = _tab.length; _i < _len; _i++ )
	// {
	// 	_callback( _tab[ _i ], _i, _len );
	// }

	_tab.foreach( function( _data )
	{
		_callback( _data );
	} );
}


//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// UTILity Functions
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//


//
// Helper to stop redirect on link clicking...
//
Acecool.util.StopEvent = function( _event )
{
	if( !_event )
		var _event = window.event;

	// Internet Explorer Support
	_event.cancelBubble = true;
	_event.returnValue = false;

	//
	if ( _event.preventDefault )
		_event.preventDefault( );

	// Firefox Support
	if ( _event.stopPropagation )
		_event.stopPropagation( );

	//
	return false;
};


//
// Helper - Returns the host ( domain without sub-domain or anything after .com etc.. portion of domain )
//
Acecool.util.GetHost = function( _event )
{
	return $( location ).attr( 'host' );
};


//
//
//
Acecool.util.CreateCookie = function( _name, _value, _days )
{
	var _expires = "";

	//
	if ( _days )
	{
		//
		var _date = new Date( );

		//
		_date.setTime( _date.getTime( ) + ( _days * 24 * 60 * 60 * 1000 ) );

		//
		_expires = "; expires=" + _date.toGMTString( );
	}

	//
	document.cookie = _name + "=" + _value + _expires + "; path=/";
}


//
// Helper allowing you to loop through each vararg and use a callback function to manipulate the data - also allows you to skip n number of arguments so the first can be skipped, etc...
//
Acecool.util.ForEachVarArgs = function( _varargs, _skip, _func )
{
	// Number of args including non-varargs..
	var _count = _varargs.length;

	// Debugging:
	// console.log( 'Calling Acecool.util.ForEachVarArgs: _func is ' + typeof( _func ) + ' isfunction( _func ) == ' + isfunction( _func ) );
	// console.log( 'Calling Acecool.util.ForEachVarArgs: _varargs count is ' + _count );
	// console.log( 'Calling Acecool.util.ForEachVarArgs: _skip is ' + _skip );

	// If we have data to process...
	if ( _count > _skip && isfunction( _func ) )
	{
		// Then, for each which we process, call our callback on it..
		for ( _i = _skip; _i < _count; _i++ )
		{
			_func( _varargs[ _i ], _i );
		}
	}
}


//
// Does the same as ForEachVarArgs but delimits them by 2 so you end up with key / value return on each call...
//
Acecool.util.ForEachKeyValueVarArgs = function( _varargs, _skip, _func )
{
	// Number of args including non-varargs..
	var _count = _varargs.length;

	// Debugging:
	// console.log( 'Calling Acecool.util.ForEachVarArgs: _func is ' + typeof( _func ) + ' isfunction( _func ) == ' + isfunction( _func ) );
	// console.log( 'Calling Acecool.util.ForEachVarArgs: _varargs count is ' + _count );
	// console.log( 'Calling Acecool.util.ForEachVarArgs: _skip is ' + _skip );

	// If we have data to process...
	if ( _count > _skip && ( ( _count - _skip ) % 2 == 0 ) && isfunction( _func ) )
	{
		// Then, for each which we process, call our callback on it..
		for ( _i = _skip; _i < _count; _i = _i + 2 )
		{
			// Debugging
			// alert( 'ForEachOtherVarArgs: Key: ' + _varargs[ _i ] + ' / ' + _varargs[ _i + 1 ] );

			// Callback
			_func( _varargs[ _i ], _varargs[ _i + 1 ] );
		}
	}
}


//
// Creates basic Getter and Setter functions
//
Acecool.util.AccessorFunc = function( _tab, _name, _default, _getter_prefix = 'Get', _key = _name )
{
	// This variable holds the value reference for these AccessorFuncs
	// var _data = _default;
	if ( !_tab.__data )
		_tab.__data = { };

	//
	_key = _name.toLowerCase( )

	//
	_tab.__data[ _key ] = _default;


	//
	//
	//
	_tab[ _getter_prefix + _name ] = function( _default )
	{
		return _tab.__data[ _key ];
	};


	//
	//
	//
	_tab[ 'Set' + _name ] = function( _value )
	{
		_tab.__data[ _key ] = _value;
	};
}


//
// Creates basic function
//
Acecool.util.AccessorSimple = function( _tab, _name, _default, _key )
{
	// This variable holds the value reference for these AccessorFuncs
	var _data = _default;


	//
	//
	//
	_tab[ _getter_prefix + _name ] = function( _default )
	{
		return _data;
	};


	//
	//
	//
	_tab[ 'Set' + _name ] = function( _value )
	{
		_data = _value;
	};
}


//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// html extensions
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//




//
// Helper - Returns whether the object is visible or not..
//
Acecool.html.IsVisible = function( _element )
{
	// Check specifics for hidden..
	var _hidden = _element.is( ':hidden' ) || _element.css( 'visibility' ) == 'hidden' || _element.css( 'opacity' ) == '0' || _element.css( 'display' ) == 'none';

	// And Visible
	var _visible = _element.is( ':visible' );

	return !_hidden || _visible;
};


//
// Helper - Returns whether the object is visible or not..
//
Acecool.html.IsHidden = function( _element )
{
	return !Acecool.html.IsVisible( _element );
};


//
// Helper to retrieve the current website / document source-code...
//
Acecool.html.GetSiteSource = function( )
{
	return document.getElementsByTagName( 'html' )[ 0 ].outerHTML;
};


//
//
//
Acecool.html.CreateElement = function( _type, _options )
{

	// Grab the element on the page where we want to insert the download information...
	// var _element = document.getElementById( "ItemControls" );

	// A simple Line Break to add...
	// var _br = document.createElement( 'hr' );
	// _br.setAttribute( 'height', '1px' );

	// The download progress bar...
	// var _progress = document.createElement( 'progress' );
	// _progress.setAttribute( 'id', 'progress_' + _wsid );
	// _progress.setAttribute( 'value', '0' );

	// Create an img tag to display the addon preview-image next to the download info...
	// var _img = document.createElement( 'img' );
	// _img.setAttribute( 'id', 'progress_img_' + _wsid );
	// _img.setAttribute( 'width', 64 );
	// _img.setAttribute( 'src', _addon_photo );
	// _img.setAttribute( 'alt', 'Addon: ' + _addon_title + ' will be saved as: ' + _addon_clean_filename );

	// Create a span tag to house information to display to the client in terms of how far along the download is...
	// var _info = document.createElement( 'span' );
	// _info.setAttribute( 'id', 'progress_info_' + _wsid );
	//_info.setAttribute( 'class', 'workshopItemDescription' );
	// _info.innerHTML = 'Download Data!';

	// Add the elements to where we want them to appear on the page...
	// _element.parentNode.appendChild( _br, _element );
	// _element.parentNode.appendChild( _img, _element );
	// _element.parentNode.appendChild( _progress, _element );
	// _element.parentNode.appendChild( _info, _element );
	//_element.parentNode.appendChild( _br, _element );
	//var _element =
	//<div class="act-st" style=""><img src="https://secureir.ebaystatic.com/cr/v/c1/ColwebImages/throbber_15px.gif" width="15" alt="Loading..." title="Loading..."><span>Adding...</span></div>

	var _e = document.createElement( _type );
	var _tag = 'id';
	if ( _options )
	{
		_tag = 'id';
		if ( _options[ _tag ] )
			_e.ssetAttribute( _tag, ( _options[ _tag ] ) );

		_tag = 'class';
		if ( _options[ _tag ] )
			_e.ssetAttribute( _tag, ( _options[ _tag ] ) );

		_tag = 'value';
		if ( _options[ _tag ] )
			_e.ssetAttribute( _tag, ( _options[ _tag ] ) );

		_tag = 'width';
		if ( _options[ _tag ] )
			_e.ssetAttribute( _tag, ( _options[ _tag ] ) );

		_tag = 'height';
		if ( _options[ _tag ] )
			_e.ssetAttribute( _tag, ( _options[ _tag ] ) );

		_tag = 'src';
		if ( _options[ _tag ] )
			_e.ssetAttribute( _tag, ( _options[ _tag ] ) );

		_tag = 'alt';
		if ( _options[ _tag ] )
			_e.ssetAttribute( _tag, ( _options[ _tag ] ) );

		// if ( _options.id )
		// 	_e.ssetAttribute( 'id', ( _options.id ) );

		// if ( _options.value )
		// 	_e.ssetAttribute( 'value', ( _options.value ) );

		// if ( _options.xxxx )
		// 	_e.ssetAttribute( 'xxxx', ( _options.xxxx ) );

		// if ( _options.h || _options.height )
		// 	_e.ssetAttribute( 'height', ( _options.h || _options.height ) );

		// if ( _options.w || _options.width )
		// 	_e.ssetAttribute( 'width', ( _options.w || _options.width ) );
	}

	//
	return _e;
};



//
//
//
Acecool.html.AddLoadingElements = function( _anchor )
{

	//<div class="act-st" style=""><img src="https://secureir.ebaystatic.com/cr/v/c1/ColwebImages/throbber_15px.gif" width="15" alt="Loading..." title="Loading..."><span>Adding...</span></div>
};


//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// HTTP
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//


//
// HTTP Fetch using GM_xmlhttpRequest
//
Acecool.http.Fetch = function( _url, _onsuccess, _onfail, _onprogress, _onreadystatechange, _method = "GET", _ignore_cache = true, _redirect_limit = 0 )
{
	if ( _url.substring( 0, 4 ) != 'http' )
	{
		console.log( "[ Acecool ][ Functions Lib ][ http > GetTitle ] Error: _url doesn't begin with http!" );
		if ( _onsuccess )
			_onsuccess( "Error: _url doesn't begin with http!!!" );

		if ( _onfail )
			_onfail( "Error: _url doesn't begin with http!!!" );

		return null;
	}

	//
	// Request the data and add the OnClick functionality to our DownloadBtn for Downloading ALL collection items...
	//
	GM_xmlhttpRequest( {
		method: _method,
		url: _url,
		ignoreCache: _ignore_cache,
		redirectionLimit: _redirect_limit,
		// method: "GET",
		// //method: "POST",
		// url: _url,
		// ignoreCache: true,
		// redirectionLimit: 0,

		//
		// Header data to send...
		//
		headers:
		{
			"Content-Type": "text/plain;charset=UTF-8",
		},

		//
		// Called when the Ready State has Changed... ( 1 = Init, 2 = Unknown, 3 = Progress Callback, 4 = Completed )
		//
		onreadystatechange: function( _response )
		{
			if ( _onreadystatechange )
				_onreadystatechange( _response );
		},


		//
		// Called when data-state has changed.. ie progress has been made downloading the addon.. ( Same as ReadyStateChange ID 3 )
		//
		onprogress: function( _event )
		{
			if ( _onprogress )
				_onprogress( _event );
		},


		//
		// On Completed Callback... ( Same as ReadyStateChange ID 4 )
		//
		onload: function( _data )
		{
			if ( _onsuccess )
				_onsuccess( _data.responseText, _data );
		},


		//
		// Error Callback
		//
		onerror: function( _error )
		{
			if ( _onfail )
				_onfail( _error );
		}
	} );
};


//
//
//
Acecool.http.GetTitle = function( _url, _onsuccess, _onfail )
{
	if ( _url.substring( 0, 4 ) != 'http' )
	{
		console.log( "[ Acecool ][ Functions Lib ][ http > GetTitle ] Error: _url doesn't begin with http!" );
		// if ( _onsuccess )
		// 	_onsuccess( "Error: _url doesn't begin with http!!!" );

		if ( _onfail )
			_onfail( "Error: _url ' + _url + ' doesn't begin with http!!!" );

		return null;
	}

	//
	// var _fetch_url = "http://localhost/get_external_title.php?url=" + _url;
	var _fetch_url = _url;

	Acecool.http.Fetch( _url, function( _response, _data )
	{
		if ( _onsuccess )
		{
			var _title = $( _response ).filter( 'title' ).text( );

			_onsuccess( _title, _response, _data );
		}
	}, function( _error )
	{
		if ( _onfail )
			_onfail( _error );
	} );

	// //
	// $.ajax( {
	// 	//
	// 	url: _fetch_url,

	// 	//
	// 	async: true,

	// 	//
	// 	success: function( _data )
	// 	{
	// 		if ( _onsuccess )
	// 		{
	// 			var _title = $( _data ).filter( 'title' ).text( );

	// 			_onsuccess( _title, _data );
	// 		}
	// 	},

	// 	//
	// 	error: function( _error ) {
	// 		if ( _onfail )
	// 			_onfail( _error );
	// 	}
	// } );
}


//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// Acecool Steam API
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//

//
Acecool.SteamAPI = { };

// var Acecool.Steam = Acecool.SteamAPI;
// var Acecool.steam = Acecool.SteamAPI;
// var Acecool.steamapi = Acecool.SteamAPI;
// var Acecool.steam_api = Acecool.SteamAPI;


//
//
//
const CFG_STEAM_API_KEY						= 'AC5F21543990B290C5C7A398B16A2F4B';
const CFG_STEAM_API_QUERY_STEAMID64			= '76561197968611839';
const CFG_STEAM_API_QUERY_PROFILE_ID		= 'Acecool';
const CFG_STEAM_API_QUERY_FORMAT			= 'json';

//
// Enumerations
//
const STEAM_API_KEY 						= 1;
const STEAM_API_FORMAT 						= 2;
const STEAM_API_STEAMID 					= 3;
const STEAM_API_STEAMID64 					= 4;
const STEAM_API_GAMEID 						= 5;
const STEAM_API_RESULTS 					= 6;


//
// Banned Enums
//
const STEAM_API_VAC_BANNED					= 1;
const STEAM_API_ECONOMY_BAN					= 2;
const STEAM_API_COMMUNITY_BAN				= 3;


//
// More Enumerations: https://developer.valvesoftware.com/wiki/Steam_Web_API
//
const STEAM_API_PERSONSTATE_OFFLINE			= 0;
const STEAM_API_PERSONSTATE_ONLINE			= 1;
const STEAM_API_PERSONSTATE_BUSY			= 2;
const STEAM_API_PERSONSTATE_AWAY			= 3;
const STEAM_API_PERSONSTATE_SNOOZE			= 4;
const STEAM_API_PERSONSTATE_LOOKTOTRADE		= 5;
const STEAM_API_PERSONSTATE_LOOKTOPLAY		= 6;

const STEAM_API_PROFILE_VISIBILITY_PRIVATE	= 1;
const STEAM_API_PROFILE_VISIBILITY_PUBLIC	= 3;





//
// The helper-function which handles ALL steam requests using JSON format...
//
Acecool.SteamAPI.FetchData = function( _urlstring, _varargs, _callback, _custom_url )
{
	// Log it..
	Acecool.SteamAPI.Log( 'FetchData', 'Attempting to query!' );

	//
	var _query_url = ( !_custom_url ) ? 'http://api.steampowered.com/' : null;

	// local _url = ( !_custom_url ) && string.format( _query_url .. _urlstring, unpack( ( _varargs || { } ) ) ) .. "&format=json" || nil;
	// Task: Finish coding Acecool.SteamAPI.FetchData...
	// var _url = ( !_custom_url ) ? _query_url.format( ): null;

	// Fetch the data..
	Acecool.http.Fetch( ( ( !_custom_url ) ? _url : _urlstring ), function( _data )
	{
		// Log it...
		Acecool.SteamAPI.Log( 'FetchData', 'Received successful response from query!' );

		// Convert the JSON String to an Object / Array
		var _raw = JSON.parse( _data );

		// If we have a callback - run it... // ( _data && _custom_url ) || ( !_custom_url && _raw &&
		if ( isfunction( _callback ) && isstring( _raw ) )
			_callback( _raw, _data );
		else
			console.log( ' >> Acecool.SteamAPI.FetchData Error - RawData is unset or no callback was provided!' );
	} );
}


//
// simple helper function
//
Acecool.SteamAPI.BuildVersion = function( _version = 1 )
{
	return 'v000' + _version.toString( ) + '/';
}


//
// Returns games owned by the player
// Task: Most important function to code!!!
//
Acecool.SteamAPI.PlayerGames = function( _steam = CFG_STEAM_API_QUERY_STEAMID64, _callback = null ) // function( _count, _games, _raw ) { } )
{
	// Translate SteamID
	var [ _steamid, _steamid64, _steamuid ] = Acecool.SteamAPI.TranslateSteamID( _steam );

	// If we're targeting a bot, stop it...
	if ( _steamid === 'BOT' )
		return false;

	// Set up the url data...
	// var _varargs = { CFG_STEAM_API_KEY, _steamid64, 1, 1 };

	/*

	local _varargs = { STEAM_API_KEY, _steamid64, 1, 1 };
	local _urlstring = 'IPlayerService/GetOwnedGames/v0001/?key=%s&steamid=%s&include_appinfo=%s&include_played_free_games=%s';
	self:FetchData( _urlstring, _varargs, function( _raw )
		local _response = _raw.response;
		local _game_count = _response.game_count;
		local _error = ( !_response || table.Count( _response ) == 0 || !_game_count )

		if ( _error ) then
			_error = 'Unknown Profile or no games ever owned..';
			if ( isfunction( _error_callback ) ) then
				_error_callback( _error );
			else
				print( 'Error: ' .. _error );
			end

			return;
		else
			// Processed games.. game app ID set as KEY and value as rest of data...
			local _games = { };
			local _raw_games = _response.games;
			for k, v in pairs( _raw_games ) do
				_games[ v.appid ] = v;
			end

			_callback( _game_count, _games, _raw );
		end
	end );
	*/
}


//
// Returns player friends list
//
Acecool.SteamAPI.PlayerFriendsList = function( _steam = CFG_STEAM_API_QUERY_STEAMID64, _callback = null ) //function( _friends, _raw ) { } )
{
	// // Translate SteamID
	// var [ _steamid, _steamid64, _steamuid ] = Acecool.SteamAPI.TranslateSteamID( _steam );

	// // If we're targeting a bot, stop it...
	// if ( _steamid === 'BOT' )
	// 	return false;



	/*

	if ( _steamid == 'BOT' ) then return false; end
	local _varargs = { STEAM_API_KEY, _steamid64 };
	local _urlstring = 'ISteamUser/GetFriendList/v0001/?key=%s&steamid=%s&format=json';
	self:FetchData( _urlstring, _varargs, function( _raw )
		local _response = _raw.friendslist;

		if ( !_response || table.Count( _response ) == 0 ) then
			local _error = 'Private Profile, Unknown Profile or No Friends..';
			if ( isfunction( _error_callback ) ) then
				_error_callback( _error );
			else
				print( 'Error: ' .. _error );
			end

			return;
		else
			// Processed friends.. SteamID set as KEY and value as rest of data...
			local _friends = { };
			local _raw_friends = _response.friends;
			for k, v in pairs( _raw_friends ) do
				local _steam, _steam64, _steam3 = util.TranslateSteamID( v.steamid );
				v.steamid = _steam;
				v.steamid64 = _steam64;
				v.steamid3 = _steam3;
				_friends[ _steam ] = v;
			end

			_callback( _friends, _raw );
		end
	end );*/
}


//
// Lookup SteamID to see whether user is using FamilySharing / Owns the game or not...
//
Acecool.SteamAPI.PlayerFamilySharing = function ( _steam = CFG_STEAM_API_QUERY_STEAMID64, _gameid = 4000, _callback = null ) //function( _bSharing, _lenderid ) { } )
{
	// // Translate SteamID
	// var [ _steamid, _steamid64, _steamuid ] = Acecool.SteamAPI.TranslateSteamID( _steam );

	// // If we're targeting a bot, stop it...
	// if ( _steamid === 'BOT' )
	// 	return false;


	/*
	if ( _steamid == 'BOT' ) then return false; end
	local _varargs = { STEAM_API_KEY, _steamid64, _gameid };
	local _urlstring = 'IPlayerService/IsPlayingSharedGame/v0001/?key=%s&steamid=%s&appid_playing=%s';
	self:FetchData( _urlstring, _varargs, function( _raw )
		local _response = _raw.response;
		local _lenderid = _raw.response.lender_steamid;
		local _bSharing = ( _lenderid != '0' );
		_lenderid = ( !_bSharing ) && _steamid || _lenderid;
		_callback( _bSharing, _lenderid );
	end );*/

}


//
// Returns VAC Ban History
//
Acecool.SteamAPI.PlayerBanHistory = function ( _steam = CFG_STEAM_API_QUERY_STEAMID64, _callback = null ) //function( _banned, _bans, _ban_count, _days_since_last_ban, _raw ) { } )
{
	// // Translate SteamID
	// var [ _steamid, _steamid64, _steamuid ] = Acecool.SteamAPI.TranslateSteamID( _steam );

	// // If we're targeting a bot, stop it...
	// if ( _steamid === 'BOT' )
	// 	return false;

	/*

	if ( _steamid == 'BOT' ) then return false; end
	local _varargs = { STEAM_API_KEY, _steamid64 };
	local _urlstring = 'ISteamUser/GetPlayerBans/v0001/?key=%s&steamids=%s&format=json';
	self:FetchData( _urlstring, _varargs, function( _raw )
		local _response = _raw.players;
		local _data = _response[ 1 ];

		if ( !_data || table.Count( _data ) == 0 ) then
			local _error = 'Unknown Profile';			if ( isfunction( _error_callback ) ) then
				_error_callback( _error );
			else
				print( 'Error: ' .. _error );
			end

			return;
		else
			local _bans = {
				[ STEAM_API_VAC_BANNED ] = _data.VACBanned;
				[ STEAM_API_ECONOMY_BAN ] = _data.EconomyBan;
				[ STEAM_API_COMMUNITY_BAN ] = _data.CommunityBanned;
			};
			local _banned = _data.VACBanned || _data.EconomyBan || _data.CommunityBanned;
			_banned = ( _banned == 'none' ) && false || true;

			// Community = ??, vac = server, economy = trading, number of bans, last ban x days ago..
			_callback( _banned, _bans, _data.NumberOfVACBans, _data.DaysSinceLastBan, _raw );
		end
	end );*/
}


//
// Returns list of recently played games by the player
//
Acecool.SteamAPI.PlayerRecentlyPlayedGames = function( _steam = CFG_STEAM_API_QUERY_STEAMID64, _callback = null, _error_callback = null ) //function( _games, _raw ) { }, _error_callback = function( _error ) { } )
{
	// // Translate SteamID
	// var [ _steamid, _steamid64, _steamuid ] = Acecool.SteamAPI.TranslateSteamID( _steam );

	// // If we're targeting a bot, stop it...
	// if ( _steamid === 'BOT' )
	// 	return false;


/*
	if ( _steamid == 'BOT' ) then return false; end
	local _varargs = { STEAM_API_KEY, _steamid64, 10 };
	local _urlstring = 'IPlayerService/GetRecentlyPlayedGames/v0001/?key=%s&steamid=%s&count=%s';
	self:FetchData( _urlstring, _varargs, function( _raw )
		local _response = _raw.response;
		local _error = _response.total_count == 0;

		if ( _error ) then
			_error = 'Unknown Profile or no last-games ever played..';
			if ( isfunction( _error_callback ) ) then
				_error_callback( '' );
			else
				print( 'Error: ' .. _error );
			end

			return;
		else
			// Processed games.. game app ID set as KEY and value as rest of data...
			local _games = { };
			local _raw_games = _response.games;
			for k, v in pairs( _raw_games ) do
				_games[ v.appid ] = v;
			end

			_callback( _games, _raw );
		end
	end );*/
}


//
// Returns list of groups player is a part of
//
Acecool.SteamAPI.PlayerGroups = function( _steam = CFG_STEAM_API_QUERY_STEAMID64, _callback = null, _error_callback = null ) //function( _groups, _raw ) { }, _error_callback = function( _error ) { } )
{
	// // Translate SteamID
	// var [ _steamid, _steamid64, _steamuid ] = Acecool.SteamAPI.TranslateSteamID( _steam );

	// // If we're targeting a bot, stop it...
	// if ( _steamid === 'BOT' )
	// 	return false;

	/*

	if ( _steamid == 'BOT' ) then return false; end
	local _varargs = { STEAM_API_KEY, _steamid64, 10 };
	local _urlstring = 'ISteamUser/GetUserGroupList/v0001/?key=%s&steamid=%s';
	self:FetchData( _urlstring, _varargs, function( _raw )
		local _response = _raw.response;
		local _error = _response.error;
		local _success = _response.success;

		if ( !_success && _error ) then
			if ( isfunction( _error_callback ) ) then
				_error_callback( _error );
			else
				print( 'Error: ' .. _error );
			end

			return;
		else
			// Processed groups.. Groups ID set as KEY and value as true to make checking if part of group easier...
			// Just if ( _groups[ id ] ) then ... end
			local _groups = { };
			local _raw_groups = _response.groups;
			for k, v in pairs( _raw_groups ) do
				_groups[ v.gid ] = true;
			end

			_callback( _groups, _raw );
		end
	end );*/
}


//
// Returns players currently playing in game
//
Acecool.SteamAPI.GamePlayerCount = function( _game = 4000, _callback = null, _error_callback = null ) //function( _player_count, _result, _raw ) { }, _error_callback = function( _error ) { } )
{

	/*	local _urlstring = 'ISteamUserStats/GetNumberOfCurrentPlayers/v0001/?appid=' .. tostring( _game || 4000 );
	self:FetchData( _urlstring, _varargs, function( _raw )
		if ( !_raw || !_raw.response ) then
			error( 'Error fetching valid response: ' .. toprint( _raw ) );
		end

		local _result = _raw.response.result
		if ( _result == 42 ) then
			if ( isfunction( _error_callback ) ) then
				_error_callback( _result );
			else
				print( 'Error code: ' .. _result .. '. InValid Game ID!' );
			end
		else
			local _player_count = _raw.response.player_count;
			_callback( _player_count, _result, _raw )
		end
	end );*/
}


//
// Returns information about a player in this table:
// local _data = _raw.response.players.player[ 1 ];
// commentpermission, profileurl, avatar, personaname, avatarfull, personastate,
// avatarmedium, profilestate, communityvisibilitystate, lastlogoff, steamid64
//
Acecool.SteamAPI.PlayerSummary = function( _steam = CFG_STEAM_API_QUERY_STEAMID64, _callback = null, _error_callback = null ) //function( _steamid, _profile_url, _persona_name, _last_loggoff, _permission_comment, _tab_avatar, _tab_persona, _raw ) { }, _error_callback = function( _error ) { }, _version = 1 )
{
	// Translate SteamID
	var [ _steamid, _steamid64, _steamuid ] = Acecool.SteamAPI.TranslateSteamID( _steam );

	/*

	local _urlstring = 'ISteamUser/GetPlayerSummaries/' .. self:BuildVersion( _version ) .. '?key=%s&steamids=%s';
	local _varargs = { STEAM_API_KEY, _steamid64 };
	self:FetchData( _urlstring, _varargs, function( _raw )
		// V1 and 2 are identical in terms of data, but V2 only nests 2 tables instead of 3.
		local _data = nil;
		if ( !_version || _version == 1 ) then
			_data = _raw.response.players.player[ 1 ];
		elseif( _version == 2 ) then
			_data = _raw.response.players[ 1 ];
		end

		// _data.steamid which is Steam64 swapped for Steam32.
		// ( _steam, _profileurl, _name, _lastlogoff, _commentpermission, _avatardata, _statedata, _raw )
		if ( _data ) then
			_callback( _steamid, _data.profileurl, _data.personaname, _data.lastlogoff, _data.commentpermission, { avatar = _data.avatar, avatar_med = _data.avatarmedium, avatar_full = _data.avatarfull }, { state_persona = _data.personastate, state_profile = _data.profilestate, state_visibility = _data.communityvisibilitystate }, _raw );
		else
			local _error = 'steam:PlayerSummary( ' .. ( ( _steam && _steam != '' ) && _steam || 'STEAM_BLANK' ) .. ( tostring( _callback ) || 'CALLBACK_BLANK' ) .. ( _version || 1 ) .. ' ); -> FAILED!';
			if ( isfunction( _error_callback ) ) then
				_error_callback( _error );
			else
				print( _error );
			end
		end
	end );*/
}


//
// Generate Example Function - This functions houses all of the possible calls for this API to show as examples...
//
// MOTE, all SteamID FIELDS can accept any form of SteamID...
// SteamID-32 ( STEAM_0:1:4173055 ), SteamID64 ( 76561197968611839 ), SteamID3/U ( [U:1:8346111] )
// All forms of SteamID are converted using util.TranslateSteamID( x ) where x can be ANY form and
// all 3 are returned in the above mentioned order. local _steam, _steam64, _steam3 = util.TranslateSteamID( x );
//
// Note, All second callbacks ( for errors ) aren't required... They're used here to show that they exist... The first Family Sharing call is an example of how simple these are without the error callback..
//
Acecool.SteamAPI.GenerateExample = function( )
{
	// PlayerFamilySharing; NO ERROR CALLBACK. Returns true if sharing, and lenderid becomes SteamID of lender
	// but if not sharing, _lenderid is steamid of local player
	Acecool.SteamAPI.PlayerFamilySharing( 'STEAM_0:1:4173055', 4000, function( _bSharing, _lenderid )
	{
		console.log( _bSharing, _lenderid );
	} );


	// Showing PlayerBanHistory; ERROR No bans for me
	Acecool.SteamAPI.PlayerBanHistory( 'STEAM_0:1:4173055', function( _banned, _bans, _ban_count, _sober_for, _raw )
	{
		var _ban_community		= _bans[ STEAM_API_COMMUNITY_BAN ];
		var _ban_vac			= _bans[ STEAM_API_VAC_BANNED ];
		var _ban_economy		= _bans[ STEAM_API_ECONOMY_BAN ];

		console.log( 'Banned?: ', _banned );
		console.log( 'Times Banned: ', _ban_count );
		console.log( 'Sober For: ', _sober_for, ' days!' );
		console.log( 'Community Ban: ', _ban_community );
		console.log( 'VAC Ban: ', _ban_vac );
		console.log( 'Economy Ban: ', _ban_economy );

		// console.log( _banned, _bans, _ban_count, _sober_for, _raw );
	}, function( _error )
	{
		console.log( _error );
	} );


	// Showing Player Friends List; ERROR, PRIVATE PROFILE / UNKNOWN PROFILE / NO FRIENDS
	Acecool.SteamAPI.PlayerFriendsList( 'STEAM_0:1:4173055', function( _data )
	{
		console.log( _data );
	}, function( _error )
	{
		console.log( _error );
	} );


	// Showing PlayerGames; ERROR, _game_count is number of games owned, _games is table with AppID as KEY, appid, img_icon_url, img_logo_url, name, playtime_forever as value
	Acecool.SteamAPI.PlayerGames( 'STEAM_0:1:4173055111111', function( _game_count, _games, _raw )
	{
		console.log( _game_count );
	}, function( _error )
	{
		console.log( _error );
	} );


	// Showing PlayerRecentlyPlayedGames; ERROR.
	Acecool.SteamAPI.PlayerRecentlyPlayedGames( '-1', function( _games, _raw )
	{
		console.log( _games );
	}, function( _error )
	{
		console.log( _error );
	} );


	// Showing PlayerGroups; ERROR, private profile
	Acecool.SteamAPI.PlayerGroups( '76561198043337919', function( _groups )
	{
		console.log( _groups );
	}, function( _error )
	{
		console.log( _error );
	} );


	// Showing PlayerGroups; ERROR, Profile doesn't exist
	Acecool.SteamAPI.PlayerGroups( '-1', function( _groups )
	{
		console.log( _groups );
	}, function( _error )
	{
		console.log( _error );
	} );


	// Showing GamePlayerCount test; ERROR, using OnError Callback function ( would print otherwise )
	Acecool.SteamAPI.GamePlayerCount( -1, function( _player_count, _result, _raw )
	{
		console.log( _player_count, _result, _raw );
	}, function( _error )
	{
		console.log( _error );
	} );


	// Showing PlayerSummary test; ERROR, using OnError Callback function ( would print otherwise )
	Acecool.SteamAPI.PlayerSummary( 'STEAM_0:1:4173055-11111', function( _steam, _profileurl, _name, _lastlogoff, _commentpermission, _avatardata, _statedata, _raw )
	{
		console.log( _steam, _profileurl, _name, _lastlogoff, _commentpermission, _avatardata, _statedata, _raw );
	}, function( _error )
	{
		console.log( _error );
	} );
}


//
// Returns the group data based on id or name...
//
function match_xml( _data, _tag, _match, _remove )
{
	// local _xml = string.match( _data, '<' .. _tag .. '>[' .. _match .. ']+</' .. _tag .. '>' ) || '';
	// _xml = string.gsub( _xml, _remove, '' );
	// return _xml;
}


//
//
//
function strip_xml_cdata( _line, _tag )
{
	// local _linelen = string.len( _line );
	// local _len = string.len( _tag );
	// return string.sub( _line, _len + 12, _linelen - _len - 6 );
}


//
//
//
function read_xml_numeric( _data, _tag )
{
	// return match_xml( _data, _tag, '%d', '%D' );
}


//
//
//
function read_xml_cdata( _data, _tag )
{
	// return strip_xml_cdata( match_xml( _data, _tag, '%S', '%s' ), _tag );
}


//
//
//
Acecool.SteamAPI.GroupData = function ( _id, _callback )
{
/*	// Choose the url to use to fetch the group data...
	local _url = 'http://steamcommunity.com/groups/' .. ( _id ) .. '/memberslistxml/?xml=1';
	if ( string.len( string.gsub( _id, '%D', '' ) ) == string.len( _id ) ) then
		_url = 'http://steamcommunity.com/gid/' .. tostring( _id ) .. '/memberslistxml/?xml=1';
	end
	print( 'using url: ' .. _url );

	//
	self:FetchData( _url, nil, function( _, _raw )
		local _memberCount = read_xml_numeric( _raw, 'memberCount' );
		local _data = {
			groupID64 = read_xml_numeric( _raw, 'groupID64' );

			groupDetails = {
				groupName = read_xml_cdata( _raw, 'groupName' ) || '#group_name_missing';
				groupURL = read_xml_cdata( _raw, 'groupURL' ) || '#group_url_missing';
				headline = read_xml_cdata( _raw, 'headline' ) || '#group_headline_missing';
				summary = read_xml_cdata( _raw, 'summary' ) || '#group_summary_missing';
				avatarIcon = read_xml_cdata( _raw, 'avatarIcon' );
				avatarMedium	= read_xml_cdata( _raw, 'avatarMedium' );
				avatarFull = read_xml_cdata( _raw, 'avatarFull' );

				memberCount = _memberCount;
				membersInChat = read_xml_numeric( _raw, 'membersInChat' );
				membersInGame = read_xml_numeric( _raw, 'membersInGame' );
				membersOnline = read_xml_numeric( _raw, 'membersOnline' );
			};

			memberCount = _memberCount;
			totalPages = read_xml_numeric( _raw, 'totalPages' );
			currentPage = read_xml_numeric( _raw, 'currentPage' );
			startingMember = read_xml_numeric( _raw, 'startingMember' );

			members = { };
		};

		// Match members...
		local _match = string.gmatch( _raw, '<steamID64>[%d]*</steamID64>' );
		for _member in _match do
			local _member = string.gsub( _member, '%D', '' );
			local _steamid, _steamid64, _steamid3 = util.TranslateSteamID( _member );
			_data.members[ _steamid ] = { steamid32 = _steamid, steamid64 = _steamid64, steamid3 = _steamid3 };
		end

		_callback( _data );
	end, true );*/
}



//
// Helper -
//
Acecool.SteamAPI.Log = function( _id = 'None', _text = '' )
{
	console.log( '[ Acecool > SteamAPI > ' + _id + ' ] ' + _text );
}


//
// Helper -
//
Acecool.SteamAPI.SplitSteamID32 = function( _steamid32 = '' )
{
	//
	_steamid32 = _steamid32.toString( );

	// Split the ID - [STEAM_0:1:234567]
	var _octets = _steamid32.split( ':' );

	// Return STEAM_0	0/1		23456789
	return [ _octets[ 0 ], _octets[ 1 ].toNumber( ), _octets[ 2 ].toNumber( ) ];
}


//
//
//
const CONST_STEAMAPI_VALVE_STEAM_ID_64 = 76561197960265728;
Acecool.SteamAPI.TranslateSteamID32To64 = function( _steamid32 = '' )
{
	//
	_steamid32 = _steamid32.toString( );

	// Grab the SteamID32 3 Components..
	var [ _a, _b, _c ] = Acecool.SteamAPI.SplitSteamID32( _steamid32 );

	// Return the calculation..
	return ( CONST_STEAMAPI_VALVE_STEAM_ID_64 + ( _c * 2 ) + ( _b ) );
}


//
//
//
Acecool.SteamAPI.TranslateSteamID64To32 = function( _steamid64 )
{
	return 'STEAM_0:0:0';
}


//
// Takes any form of SteamID ( STEAM_0:1:4173055 / 76561197968611839 / [U:1:8346111] ) as input and returns all 3 in that order ( SteamID32Bit, SteamID64Bit, SteamID3_aka_U )
// 'SteamID32', 'STEAM_0:1:4173055',		'SteamID64', '76561197968611839',			'SteamID3', '[U:1:8346111]'
Acecool.SteamAPI.TranslateSteamID = function( _steamid )
{
	// Debugging
	console.log( ' >>> Acecool > SteamAPI > TranslateSteamID > START >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>' );

	//
	console.log( ' >>> Acecool > SteamAPI > TranslateSteamID > SteamID Provided: ' + typeof( _steamid ) + ' == ' + _steamid );

	// Make sure we cast _steamid as a string if it is a number - needed for some of the JavaScript operations...
	if ( isnumber( _id ) )
		_steamid = _steamid.toString( );

	// Set up the base return values..
	var _steamid32	= '';
	var _steamid64	= -1;
	var _steamid3	= '';

	// Helpers..
	var _steamid_no	= _steamid.replace( /[\D]/g, '' );
	var _bLenSame	= ( _steamid.length == _steamid_no.length );

	// Make sure an id was provided..
	if ( !isstring( _id ) )
		return _steamid32, _steamid64, _steamid3;

	// Debugging..
	// Acecool.SteamAPI.Log( 'TranslateSteamID', _steamid );
	console.log( '[ Acecool > SteamAPI > TranslateSteamID ] [ ' + _steamid + ' ]' );

	// Determine which id was used... Check for U / 3 variant..
	if ( _steamid.substr( 1, 1 ) === 'U' )
	{
		// We're translating U to 32 and 64...
		_steamid3 = _steamid;

		// Convert it to a number for other operations after taking only the second char onward..
		_steamid = _steamid_no.substring( 1 ).toNumber( );

		// Is Even? Grab before division..
		var _bIsEven = ( _steamid % 2 == 0 );

		// Divide and floor it..
		_steamid = Math.floor( _steamid / 2 );

		// Set _steamid32 as it has been calculated, and set _steamid64 as we have _steamid32
		_steamid32 = 'STEAM_0:' + ( _bIsEven ? 0 : 1 ) + ':' + _steamid;

		// Calculate SteamID64 from SteamID32...
		_steamid64 = Acecool.SteamAPI.TranslateSteamID32To64( _steamid32 );

		//
		console.log( ' >> _32: ' + _steamid32 + ' || _64: ' + _steamid64 + ' || _3U: ' + _steamid3 + ' || _: ' + _steamid  )

		return [ _steamid32, _steamid64, _steamid3 ];
	}
	else
	{
		// Debugging..
		// Acecool.SteamAPI.Log( 'TranslateSteamID', '[ ' + _steamid + ' ] SteamID32 or 64 Type..' );
		// console.log( ' >>> Acecool > SteamAPI > TranslateSteamID > [ ' + _steamid + ' ] SteamID32 or 64 Type.' );

		// If BOT or 0 was used, then it is a bot...
		if ( _steamid === 'BOT' || _steamid === 0 )
			return [ 'BOT', 0, 'BOT' ];

		// If the length of the input id doesn't change when all non-digits is removed then the user input SteamID 64... Otherwise it is SteamID 32...
		_steamid64 = ( _bLenSame ) ? _steamid : Acecool.SteamAPI.TranslateSteamID32To64( _steamid );
		_steamid32 = ( _bLenSame ) ? Acecool.SteamAPI.TranslateSteamID64To32( _steamid ) : _steamid32;

		// Only convert if not input.. Conversion is [U:1:( ( x * 2 ) + y )] where x and y is STEAM_0:y:x
		if ( _steamid3 === '' )
		{
			// Grab the SteamID32 3 Components..
			var [ _a, _b, _c ] = Acecool.SteamAPI.SplitSteamID32( _steamid32 );

			// If they are set...
			if ( _c && _b )
			{
				// Calculate
				_c = ( _c * 2 ) + _b;

				// Combine
				_steamid3 = '[U:1:' + _c + ']';
			}
		}
	}

	// Debugging
	// console.log( ' >>> Acecool > SteamAPI > TranslateSteamID > ENDED >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>' );

	// Return all 3 keys...
	return [ _steamid32, _steamid64, _steamid3 ];
};



//
// Translate input SteamID, SteamID64 or SteamU into SteamID, SteamID64 AND SteamU. Return in same order 32, 64, U
//
/*
function util.TranslateSteamID( _steam )
	// If UID was input, ensure it is moved to correct var and calculate Steam32.
	if ( string.sub( string.upper( _steam ), 2, 2 ) == "U" ) then
		_steamuid = _steam;

		// Remove all non-digits so we can tonumber the result below, and divide by 2
		_steam = string.gsub( _steam, "%D", "" );

		// Remove first number from 1x after strip, before: [U:1:x] and convert to number.
		_steam = tonumber( string.sub( _steam, 2 ) );

		// Even or odd?
		local _bEven = ( _steam % 2 == 0 );
		_steam = math.floor( _steam / 2 );

		// Set _steamid since we calculated it, and because we have the steamU we only need 64. Do it, and return.
		_steamid = "STEAM_0:" .. ( _bEven && "0" || "1" ) .. ":" .. _steam;
		_steamid64 = util.SteamIDTo64( _steamid );
	else
		// Check for BOT SteamID of bots...
		if ( _steam == "BOT" || _steam == 0 ) then
			return "BOT", 0, "BOT";
		end

		// Check to see if the length changes if we remove non-digits.
		// Easy way to tell Steam64 from 32 ( could also get first char, and other methods )
		if ( string.len( _steam ) == string.len( string.gsub( _steam, "%D", "" ) ) ) then
			_steamid64 = _steam;
			_steamid = util.SteamIDFrom64( _steamid64 );
		else
			_steamid64 = util.SteamIDTo64( _steamid );
		end

		// Only convert if not input.. Conversion is [U:1:( ( x * 2 ) + y )] where x and y is STEAM_0:y:x
		if ( _steamuid == "" ) then
			local _uidX = tonumber( string.sub( _steamid, 9, 9 ) );
			local _uidY = tonumber( string.sub( _steamid, 11 ) );
			if ( _uidY && _uidX ) then
				_uidY = ( _uidY * 2 ) + _uidX;
				_steamuid = "[U:1:" .. _uidY .. "]";
			end
		end
	end
end

	// Begin by multiplying by 2 ( Steam seems to do this with U too.... ) and add our VALVe constant...
	// var _steamid64 = ;
	// _steamid64 += ( _c * 2 );
	// _steamid64 += _b;
	// _steamid64 = _steamid64.toPrecision( 17 );

	// If [STEAM_0:<THIS>:123456] <THIS> is 1, then add 1.. Otherwise add 0, ie do nothing..
	// _steamid64 = _steamid64 + ( ( _b % 2 == 0 ) ? 0 : 1 );
	 
*/

//
//
//
Acecool.SteamAPI.DownloadWSID = function( _wsid )
{
	alert( "Download WSID: " + _wsid );
};


//
//
//
Acecool.SteamAPI.IsCollection = function( _wsid )
{

};






//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// Acecool.Data...
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//


// //
// //
// //
// function Acecool.data.Set( _flag, _value )
// {
// 	Acecool.__data[ _flag ] = _value;
// }


// //
// //
// //
// function Acecool.data.Get( _flag, _default )
// {
// 	var _data = Acecool.__data[ _flag ];

// 	return isset( _data ) ? _data : _default;
// }










//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// SNIPPETS / Debugging / Old-Code
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//


//
//
//


/*
Object.prototype.insertAfter = function( newNode )
{
    this.parentNode.insertBefore( newNode, this.nextSibling );
}; //*/

/*
function insertAfter(newNode, referenceNode)
{
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
} //*/


//
// --------------------------------------------------------------------------------------------------------------------------------- //
//
// Other Authors
//
// --------------------------------------------------------------------------------------------------------------------------------- //
//


//
// Base64 Encode / Decode - Written by unknown...
//
var Base64 = {
        _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
        encode: function(e) {
            var t = "";
            var n, r, i, s, o, u, a;
            var f = 0;
            e = Base64._utf8_encode(e);
            while (f < e.length) {
                n = e.charCodeAt(f++);
                r = e.charCodeAt(f++);
                i = e.charCodeAt(f++);
                s = n >> 2;
                o = (n & 3) << 4 | r >> 4;
                u = (r & 15) << 2 | i >> 6;
                a = i & 63;
                if (isNaN(r)) {
                    u = a = 64
                } else if (isNaN(i)) {
                    a = 64
                }
                t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
            }
            return t;
        },
        decode: function(e) {
            var t = "";
            var n, r, i;
            var s, o, u, a;
            var f = 0;
            e = e.replace(/[^A-Za-z0-9+/=]/g, "");
            while (f < e.length) {
                s = this._keyStr.indexOf(e.charAt(f++));
                o = this._keyStr.indexOf(e.charAt(f++));
                u = this._keyStr.indexOf(e.charAt(f++));
                a = this._keyStr.indexOf(e.charAt(f++));
                n = s << 2 | o >> 4;
                r = (o & 15) << 4 | u >> 2;
                i = (u & 3) << 6 | a;
                t = t + String.fromCharCode(n);
                if (u != 64) {
                    t = t + String.fromCharCode(r)
                }
                if (a != 64) {
                    t = t + String.fromCharCode(i)
                }
            }
            t = Base64._utf8_decode(t);
            return t;
        },
        _utf8_encode: function(e) {
            e = e.replace(/rn/g, "n");
            var t = "";
            for (var n = 0; n < e.length; n++) {
                var r = e.charCodeAt(n);
                if (r < 128) {
                    t += String.fromCharCode(r)
                } else if (r > 127 && r < 2048) {
                    t += String.fromCharCode(r >> 6 | 192);
                    t += String.fromCharCode(r & 63 | 128)
                } else {
                    t += String.fromCharCode(r >> 12 | 224);
                    t += String.fromCharCode(r >> 6 & 63 | 128);
                    t += String.fromCharCode(r & 63 | 128)
                }
            }
            return t;
        },
        _utf8_decode: function(e) {
                var t = "";
                var n = 0;
                var r = c1 = c2 = 0;
                while (n < e.length) {
                    r = e.charCodeAt(n);
                    if (r < 128) {
                        t += String.fromCharCode(r);
                        n++
                    } else if (r > 191 && r < 224) {
                        c2 = e.charCodeAt(n + 1);
                        t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                        n += 2
                    } else {
                        c2 = e.charCodeAt(n + 1);
                        c3 = e.charCodeAt(n + 2);
                        t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                        n += 3
                    }
                }
                return t;
        }
};