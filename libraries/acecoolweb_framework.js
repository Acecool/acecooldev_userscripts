// ==UserScript==
// @name			Acecool - Video Site - Ultimate Video Site Management, Ad Removal, Redirection, Direct-Linking and more!
// @author			Acecool
// @namespace		Acecool
// @version			0.0.2
// @description		Replaces encoded-links with decoded direct-links on episode finder sites.
// @description		Automatically click the "continue" button, and attempt to skip the countdown if any, on video watching sites.
// @description		Remove ad panels on video watching sites.
// @match			http://*/*
// @require			http://code.jquery.com/jquery-latest.js
// @require			http://localhost/AcecoolGit/acecooldev_userscripts/libraries/acecool_functions_lib.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/libraries/acecool_functions_lib.js
// @require			file:///C:/AcecoolGit/acecooldev_userscripts/video_sites/video_site_ultimate_tool.js
//
// @grant			GM_xmlhttpRequest
// @grant			GM_getResourceURL
//
// @xequire			file:///C:\AcecoolGit\acecooldev_userscripts\video_sites\video_site_ultimate_tool.js
// @xequire			https://bitbucket.com/acecooldev_userscripts/raw/master/libraries/acecool_functions_lib.js
// @xequire			http://localhost/AcecoolGit/acecooldev_userscripts/video_sites/video_site_ultimate_tool.js
// ==/UserScript==


//
// This script was written to solve a problem I was having with standard script development in order to prevent code duplication as every script I wrote for different sites were all sharing the same 100 lines or more of code
//	to execute on anchors, and to listen for updates, etc... And having more than 1 script set up for each site doing different tasks but having the same code increases overhead and work-load. I don't have much usable time in
//	my life due to an automobile accident which occurred on May 7, 2011 which left me with a broken neck, broken back, and severe nerve damage. The broken neck wasn't noticed despite the symptoms being present as the upper and
//	lower discs were still partially connected so in the resting position it wasn't possible to see and they don't do traction ( not that I could even handle that ) on x-rays or in MRIs. I highly recommend you to think for
//	yourself and not be afraid to demand a doctor to help you instead of blindly following their recommendations because they "practice", medicine isn't something that is repeatable for everyone as every single person is
//	different and experiences different symptoms for similar events.. Due to my injuries, I'm lucky to get the same amount of work someone would be able to do in one week in 6 months to a year. I'm in constant pain to the point
//	that it could be considered torture, and I am severely depressed. To deal with this I need to feel useful so everything I do outside of treatment is aimed at staying sane, writing code or doing things to make time spent in
//	certain areas to drop dramatically making my time more efficient. This tool helps with that.
//
// Some of the tools which are provided with this are the following:
//
//	- Default
//		+ Default is required - if a site tag is added and site tag tasks are enabled but the function isn't then it falls through to the default function( s ) to prevent issues. Additionally, because TASK_DEFAULT is the primary
//			routing system to call other functions - you don't need to create them for each site tag you create.. The default tag can route your calls properly but the option is there, if you want it, to create your own TASK_DEFAULT
//			function for your site - just remember, if you do, that if you don't route the calls then they will not run... If you create TASK_DEFAULT function then you need to write in the switch to either execute the code by either
//			redirecting tasks through the switch to unique functions, run the code in the switch, or add the code which exists in the TAG_DEFAULT TASK_DEFAULT function, which is a few lines, to automatically route the calls to the
//			appropriate unique function... Failure to do this, again - if you create a TASK_DEFAULT declaration for your TAG_X then you control what gets called and what doesn't - especially if you don't add the clause for it..
//
//			Example: Just to slam the idea home, and then some, if you create a TASK_DEFAULT for your TAG_X and leave it blank but create a TASK_* for TAG_X then TASK_* won't be called because TAG_DEFAULT won't run its TASK_DEFAULT
//					 because TASK_X TAG_DEFAULT exists. TASK_DEFAULT is the router - maybe I'll rename it to TASK_ROUTER or TASK_FUNC_ROUTER .. ideas?
//
//	- LocalHost
//		+ Basically the functions are in place for testing purposes...
//
//	- eBay AJAX Technology enabled Shopping Cart
//		+ I have just started working on this... I haven't added support to move the div elements around or remove them but this is all planned. Right now you can click one element at a time and wait for it to complete - if you
//			click too many at once, it may not complete - this is an issue with eBay and their session management so I need to create a queue to allow you to click as fast as possible and to process the jobs as soon as the last
//			is finished to avoid errors where the job reports completed ( again, an eBay issue because the eBay page reports success but it isn't saved ) but isn't. I also need to look at how eBay saves the shopping cart - if it
//			is saved via Cookies then I can manipulate it much much faster than relying on eBay. If it ins't and I have to rely on the site then I will need to find the fastest way to communicate with the eBay site such as using
//			the mobile version, or using light-weight functions which stops loading the page after 100 bytes or so - enough to allow the job to process but the smallest possible amount to allow it to happen faster - it already
//			handles quickly because you don't have to wait until the page reloads for the changes to occur but it could be better and I still have the visuals to update which, as said, is planned... pluss more.
//
//			I also plan onturning this into an addon so instead of relying on TamperMonkey or GreaseMonkey or other addons to run the scripts, it can run by itself. This will also allow me to add visual information for sites..
//			For example: When on eBay the icon can change to an eBay shopping cart and when you click on it, your cart appears, etc... ui per site to control the configuration, or as an easier way to use the script...
//
//	- BitBucket Site Adjustments
//		+ When viewing the source-directories of a project, instead of the link you click on being filled with a lot of un-needed information such as the current id / build for the project, every link uses the MASTER breanch
//			so the links are shorter and easier to share. If you are in history mode, then the full link can still be shared along with older branches so you're not stuck with this on all the time - it works with the site to
//			ensure you have the best possible experience. Also, because it works with the sites AJAX loading of the directories, when the source-container is updated, the script runs on the new links so you don't have to
//			refresh the page in order to keep viewing the master links - this is done by using observers. I also add a visual cue to the link so you know the link has been modified - it takes a split-second the first time the
//			page loads because all of the links need to be present but each subsequent viewing is applied instantly so the new links which appear are already modified! I also plan on extending support of this system to show,
//			and you can use a config value to choose whether or not you want this feature, the file-type icons instead of the standard single-icons for files vs folders...
//
//
// Note: Eventually, I will change the arguments for each task and more features for the entire system so be prepared for changes! This system was designed so you can use this script as a script that loads on all
// 		 sites, and then have other scripts with the rules load for each specific site ( or added to this script, but better if it isn't so you can allow auto-updating without losing your additions ), or add all
//		 of the rules to a separate single script - again to allow auto-updating on this script without losing your work!
//


//
// Global Script Config
//

// Note: Currently there are no global configuration options available - when they do arrive, they'll be here...

// Config X
// const CFG_X = 'x';

// Config Y
// const CFG_Y = 'y';


//
// Helper / Callback - Executed through the main script to load the global configuration data so the function can be up at the top...
//
function SetupConfiguration( )
{
	// Let the dev know that the script is running..
	console.log( ' >> AcecoolWeb_Framework > SetupConfiguration( );' );

	// Set the cycle so it doesn't load again..
	SetCycle( 'SetupConfig', 1 );

	// Setup Global Config..
	SetupGlobalConfig( 'SteamAPIKey', 'XXXXXXXXXXXXXXXXXX' );

	// SetupGlobalConfig( 'AvailableFormats', 'JSON, VDF, CSV, XML' );
	SetupGlobalConfig( 'FormatAPI', 'json' );

	// Setup User Information - Note, only SteamID64 really matters along with the Profile ID / Name..
	SetupGlobalConfig( 'SteamProfileID', 'Acecool', 'SteamID32', 'STEAM_0:1:4173055', 'SteamID64', '76561197968611839', 'SteamID3', '[U:1:8346111]' );

	// This is the url to redeem Steam keys..
	SetupGlobalConfig( 'SteamKeyRedeemURL', 'https://store.steampowered.com/account/registerkey?key=' );
	SetupGlobalConfig( 'SteamKeyAccountLicensesURL', 'https://store.steampowered.com/account/licenses/' );

	// API URLS
	SetupGlobalConfig( 'SteamAPIGetOwnedGamesURL', 'http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=' );

}

//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// Helper - Setup Cycler...
//
function InitCycle( _name )
{
	__DATA.cycles[ _name ] = { };
	__DATA.cycles[ _name ].cycling = false;
	__DATA.cycles[ _name ].cycles = 0;
}


//
// Helper - Generates a new number each time it is called...
//
var __ENUM_VALUE = -2;
function ENUM( )
{
	__ENUM_VALUE = __ENUM_VALUE + 1;
	// console.log( ' >>>>>>>>>>>> ENUM CALLED ' + __ENUM_VALUE );
	return __ENUM_VALUE;
}


//
// Helper to create the enumeration with map quicker
//
function InitializeTask( _id, _name, _desc )
{
	//
	var _id = ENUM( );

	//
	MAP_TASKS[ _id ] = _name;
	MAP_TASKS_DESC[ _id ] = _desc;

	//
	return _id;
}


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// Declarations
//

// NameSpace
var RunTime									= { };

// Data is used for certain AccessorFunc ( Automatic creation for Getter / Setter ) functions which are manually created without the use of an AccessorFunc..
var __DATA									= { };
__DATA.cycles								= { };

// Map site host-names to site TAG_* id - MAP_SITE_URLS[ 'example.com' ] = TAG_EXAMPLE; and TAG_EXAMPLE = -1; for example..
var MAP_SITE_URLS							= { };

// Map which site tasks are enabled - MAP_SITE_TASKS[ TAG_EXAMPLE ][ TASK_ON_PRELOAD ] = true; means ON_PRELOAD task is executed...
var MAP_SITE_TASKS							= { };

// Map key / value config data for sites - MAP_SITE_CFG[ TAG_EXAMPLE ][ _key ] = _value;
var MAP_SITE_CFG							= { };

// Map key / value config data globally - Although the local site takes priority...
var MAP_GLOBAL_CFG							= { };

// Maps TASK_* to a human-readable name..
var MAP_TASKS								= { };

// Maps TASK_* to a human-readable description..
var MAP_TASKS_DESC							= { };

// Contains all of the site specific features - SiteSpecificFeatures[ MAP_SITE_URLS[ TAG_* ] + '_' + MAP_TASK[ TAG_* ] ] = function( args ) { ... }
var SiteSpecifcFeatures						= { };


//
// Task Constants - Tasks are basically ids used to reference when certain code executes. On Preload executes immediately when the script loads, and OnLoad is called when $ document.onload is run, and DEFAULT is for the routing-function which isn't necessary for each site as the default / pass-through system will automatically route enabled tasks to the appropriate function, if it exists, for the site...
//

//
const TASK_DEFAULT							= InitializeTask( 'default',				'default is the primary driver of the system - if it exists for any sub-group / site then it is checked instead of the default system redirecting so everything can be housed in 1 function or separately...' );
const TASK_ON_PRELOAD						= InitializeTask( 'OnPreload',				'OnPreload is called as soon as the script loads' );
const TASK_ON_DOCUMENT_READY				= InitializeTask( 'OnDocumentReady',		'OnDocumentReady is called as soon as the page is completely loaded!' );
const TASK_ON_LOAD							= InitializeTask( 'OnLoad',					'OnLoad is called as soon as the page has completely loaded!' );
const TASK_ON_FRAME_LOAD					= InitializeTask( 'OnLoadFrame',			'OnLoadFrame is called as soon as the iframe has finished loading!' );

const TASK_PRE_PROCESS_ANCHORS				= InitializeTask( 'PreProcessAnchors',		'Called prior to ProcessAnchors being called - used to set up or reset a list to manage modifying anchors, for example!' );
const TASK_PROCESS_ANCHORS					= InitializeTask( 'ProcessAnchors',			'ProcessAnchors is called for each anchor on the site!' );
const TASK_POST_PROCESS_ANCHORS				= InitializeTask( 'PostProcessAnchors',		'Called when ProcessAnchors has been completed - used for following up to the anchor logic ie calculating largest file-sizes and recoloring the links, etc..!' );

// const TASK_PRE_PROCESS_DIVS
const TASK_PROCESS_DIVS						= InitializeTask( 'ProcessDivs',			'ProcessDivs is called for each div element on the site!' );
// const TASK_POST_PROCESS_DIVS

// const TASK_PRE_PROCESS_FRAMES
const TASK_PROCESS_FRAMES					= InitializeTask( 'ProcessFrames',			'ProcessFrames is called for each iframe element on the site!' );
// const TASK_POST_PROCESS_FRAMES

const TASK_ON_OBSERVER_MUTATION_CALLBACK	= InitializeTask( 'OnObserverMutation',		'OnObserverMutation is called when a mutation occurs!' );
const TASK_SETUP_OBSERVER					= InitializeTask( 'SetupMutationObserver',	'SetupMutationObserver is called to define what happens when a mutation occurs!' );


//
// Process Cyclers
//
InitCycle( 'default' );
InitCycle( 'anchors' );
InitCycle( 'divs' );
InitCycle( 'iframes' );


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// Task Definitions
//


//
// Helper Function - Sets up authorized tasks for the site TAG_* - Note, these are specific events...
//
function SetupTasks( _tag, _preload, _doc_ready, _on_load, _on_iframe_load, _setup_observer )
{
	MAP_SITE_TASKS[ _tag ][ TASK_ON_PRELOAD ]								= _preload;
	MAP_SITE_TASKS[ _tag ][ TASK_ON_DOCUMENT_READY ]						= _doc_ready;
	MAP_SITE_TASKS[ _tag ][ TASK_ON_LOAD ]									= _on_load;
	MAP_SITE_TASKS[ _tag ][ TASK_ON_FRAME_LOAD ]							= _on_iframe_load;
	MAP_SITE_TASKS[ _tag ][ TASK_SETUP_OBSERVER ]							= _setup_observer;
}


//
// Helper Function - Sets up authorized processes for the site TAG_* - Note, these are processes which execute EVERY TASK_* if authorized..
//
function SetupProcesses( _tag, _process_anchors, _process_divs, _process_iframes, _process_mutation_observer )
{
	// Anchor Processes...
	MAP_SITE_TASKS[ _tag ][ TASK_PRE_PROCESS_ANCHORS ]						= _process_anchors;
	MAP_SITE_TASKS[ _tag ][ TASK_PROCESS_ANCHORS ]							= _process_anchors;
	MAP_SITE_TASKS[ _tag ][ TASK_POST_PROCESS_ANCHORS ]						= _process_anchors;

	//
	// MAP_SITE_TASKS[ _tag ][ TASK_PRE_PROCESS_DIVS ]							= _process_divs;
	MAP_SITE_TASKS[ _tag ][ TASK_PROCESS_DIVS ]								= _process_divs;
	// MAP_SITE_TASKS[ _tag ][ TASK_POST_PROCESS_DIVS ]						= _process_divs;

	//
	// MAP_SITE_TASKS[ _tag ][ TASK_PRE_PROCESS_FRAMES ]						= _process_iframes;
	MAP_SITE_TASKS[ _tag ][ TASK_PROCESS_FRAMES ]							= _process_iframes;
	// MAP_SITE_TASKS[ _tag ][ TASK_POST_PROCESS_FRAMES ]						= _process_iframes;

	//
	MAP_SITE_TASKS[ _tag ][ TASK_ON_OBSERVER_MUTATION_CALLBACK ]			= _process_mutation_observer;
}


//
// Helper - Returns the task name mapped through the task enum value or it returns default if one doesn't exist...
//
function GetTaskName( _task )
{
	return MAP_TASKS[ _task ] || 'default';
}


//
// Helper - Returns the helper function name for the current TAG_* and the input task.. Optionally allow an alternate tag to be used...
//
function GetTaskFuncName( _tag, _task )
{
	// var __tag = ( _tag ) ? _tag : GetActiveTag( );
	if ( isnumber( _task ) )
	{
		if ( _task != TASK_PROCESS_ANCHORS && _task != TASK_PROCESS_DIVS &&  _task != TASK_PROCESS_FRAMES )
		{
			__Log( "[ GetTaskFuncName ]: " + _tag + '_' + GetTaskName( _task ) );
		}

		return _tag + '_' + GetTaskName( _task );
	}

	//
	if ( _task == null || !_task || _task === '' )
		return _tag;

	//
	if ( isstring( _task ) )
		return _tag + '_' + _task;

	//
	return _tag + '_' + _task.toString( );
}


//
// Helper - Returns null or the function specific to the current task..
//
function GetTaskFunc( _task )
{
	var _func =	SiteSpecifcFeatures[ GetTaskFuncName( GetActiveTag( ), _task ) ];
	return ( isfunction( _func ) ) ? _func : null;
}


//
// Helper - Returns the data-table of mapped tasks the site needs to run...
//
function GetActiveTasksTable( )
{
	return MAP_SITE_TASKS[ GetActiveTag( ) ] || null;
}


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// Tag Definitions
//


//
// Helper to initialize site TAG tables...
//
function InitializeTag( _tag )
{
	// Debugging
	// alert( arguments.length + ' ' + arguments[ 0 ] );

	// Setup tables for Configuration and tasks to execute...
	MAP_SITE_CFG[ _tag ] = { };
	MAP_SITE_TASKS[ _tag ] = { };

	// Return the tag so we can use it as a const...
	return _tag;
}


//
// Helper to assign host-names associated with a tag...
//
function SetupTagHosts( _tag, _host_repeating_as_varargs )
{
	// To keep in line with my coding standards..
	var _varargs = arguments;

	// Debugging
	// console.log( '__SetupTagURL arguments: ' + arguments );

	// If we have one or more args, ie if _host_repeating_as_varargs is set, then we can continue... or arguments.length > 1
	if ( _varargs.length > 1 )
	{
		// For Each Variable Argument starting with array index 1 ( or you can read it as skipping 1, the first which is _tag ) - add it to the site url map...
		Acecool.util.ForEachVarArgs( _varargs, 1, function( _host, _i )
		{
			// Debugging
			// alert( 'Host( ' + _i + ' ): ' + _host + ' has been added!' );

			// Add the hostname to our map...
			MAP_SITE_URLS[ _host ] = _tag;
		} );
	}

	return _tag;
}


//
// Helper to assign config key / values associated with a tag...
//
function SetupTagConfig( _tag, _cfg_key_vararg, _cfg_value_vararg )
{
	// To keep in line with my coding standards..
	var _varargs = arguments;

	// Debugging
	// console.log( '__SetupTagURL arguments: ' + arguments );

	// If we have paired args ( subtract 1 for _tag as it counts all ) then we loop through them..
	if ( ( ( _varargs.length - 1 ) / 2 )  > 0 )
	{
		// For every other index after starting point, return key as [ _i ] / value as [ _i + 1 ] to our callback function so we can add them to the cfg table...
		Acecool.util.ForEachKeyValueVarArgs( _varargs, 1, function( _key, _value )
		{
			// Debugging
			// alert( 'Config( ' + _key + ' / ' + _value + ' ) has been added!' );

			// Add the hostname to our map...
			MAP_SITE_CFG[ _tag ][ _key ] = _value;
		} );
	}

	return _tag;
}


//
// Helper to assign config key / values associated with a tag...
//
function SetupGlobalConfig( _cfg_key_vararg, _cfg_value_vararg )
{
	// To keep in line with my coding standards..
	var _varargs = arguments;

	// Debugging
	// console.log( '__SetupTagURL arguments: ' + arguments );

	// If we have paired args, then we loop through them...
	if ( ( _varargs.length / 2 ) > 0 )
	{
		// For every other index after starting point, return key as [ _i ] / value as [ _i + 1 ] to our callback function so we can add them to the cfg table...
		Acecool.util.ForEachKeyValueVarArgs( _varargs, 1, function( _key, _value )
		{
			// Debugging
			// alert( 'Config( ' + _key + ' / ' + _value + ' ) has been added!' );

			// Add the hostname to our map...
			MAP_GLOBAL_CFG[ _key ] = _value;
		} );
	}

	return _tag;
}


//
// Because of the introduction of global config - I've now set up a GetConfigValue function which looks at Tagged cfg first, and if it doesn't exist then it looks at global...
//
function GetCfg( _key, _default = null, _prefix = null, _suffix = null )
{
	// Grab the active tag..
	var _tag = GetActiveTag( );

	// Grab the active site, and global configuration tables...
	var _config_local = MAP_SITE_CFG[ _tag ];
	var _config_global = MAP_GLOBAL_CFG;

	// If the table doesn't exist for our tag, then we can't do anything with it...
	if ( isundefined( _config_local ) )
		return null;

	// If the _key isn't a string, return the tagged table...
	if ( !isstring( _key ) )
		// Return the data...
		return _config_local;

	// If our site config table has _key set, then return that data...
	var _data = _config_local[ _key ];
	if ( _data )
		// If a prefix is supplied, add it..
		if ( _prefix != null )
			_data = _prefix + _data

		// If a suffix is supplied, add it
		if ( _suffix != null )
			_data = _data + _suffix

		// Return the data...
		return _data;

	// Otherwise, look at the global table.. If it has it, then return it...
	_data = _config_global[ _key ];
	if ( _data )
		// If a prefix is supplied, add it..
		if ( _prefix != null )
			_data = _prefix + _data

		// If a suffix is supplied, add it
		if ( _suffix != null )
			_data = _data + _suffix

		// Return the data...
		return _data;

	// Seems nothing had it.. Return the default value, if set - default is null..
	if ( _default != null )
		// If a prefix is supplied, add it..
		if ( _prefix != null )
			_default = _prefix + _default

		// If a suffix is supplied, add it
		if ( _suffix != null )
			_default = _default + _suffix

		// Return the data...
		return _default;

	// Return blank...
	return '';
}


//
// Helper - Returns the TAG_* for the current site - TAG_*s are in user-friendly name format such as 'default' or 'example'.
//
function GetActiveTag( )
{
	var _tag = MAP_SITE_URLS[ Acecool.util.GetHost( ) ];
	return ( _tag ) ? _tag : TAG_DEFAULT;
}


//
// Helper - Returns the Active TAG_* Configuration Table
//
function GetActiveConfigTable( )
{
	var _cfg = MAP_SITE_CFG[ GetActiveTag( ) ];
	return _cfg;
}


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// SiteSpecificFeatures Definitions
//


//
// Helper - Return the index for the SiteSpecificFeature... if TASK_DEFAULT then leave the index as the TAG_*, otherwise the index will be _tag + '_' + _task
//
function GetFeatureIndex( _tag, _task = TASK_DEFAULT )
{
	return ( _task == TASK_DEFAULT ) ? _tag: GetTaskFuncName( _tag, _task );
}


//
// Helper function / AccessorFunc... This sets up a specific task for tag so SiteSpecificFeatures[ TAG_* + '_' + TASK_* ] = function...
//
function AddFeature( _tag, _task = TASK_DEFAULT, _func )
{
	//
	// Create a function - The name will either be TAG_* for the default / routing function or it is TAG_* + '_' + TASK_* such as default_OnPreload or example_OnPreload
	//
	SiteSpecifcFeatures[ GetFeatureIndex( _tag, _task ) ] = async function( _task_vs_data, _data_vs_cfg )
	{
		if ( isfunction( _func ) )
		{
			// _func( _task_vs_data, _data_vs_cfg );
			_func.apply( null, arguments );
		}
	}
}


//
// Helper function / AccessorFunc... Things sets up the base SiteSpecificFeature[ TAG_* ] = function
//
function GetFeature( _tag, _task = TASK_DEFAULT )
{
	return SiteSpecifcFeatures[ GetFeatureIndex( _tag, _task ) ];
}


//
// Helper - Returns the Site Specific Feature Function in charge of routing tasks to their functions OR in charge of running them == TASK_DEFAULT...
//
function GetTaskRoutingFunc( )
{
	var _func = SiteSpecifcFeatures[ GetActiveTag( ) ];
	return ( _func ) ? _func : SiteSpecifcFeatures[ TAG_DEFAULT ];
}


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// Debugging / Logging
//


//
// Helper to output console data with our prefix for easy filtering..
//
function __Log( _text, _force = true )
{
	if ( _force == true )
	{
		var _data = '[ Acecool ][ VSUM ]';
		_data += _text;

		console.log( _data );
	}
}


//
// Debug Helper - Outputs whether or not a task exists - doesn't do it for processes which repeat to avoid spamming the console until I add a controled toggle...
//
function LogDebugFuncExists( _task, _data, _cfg )
{
	if ( _task != TASK_PROCESS_ANCHORS && _task != TASK_PROCESS_DIVS &&  _task != TASK_PROCESS_FRAMES )
	{
		__Log( '[ TaskFuncExists ] ' + _task + ' / ' + GetTaskName( _task ) )
	}
}


//
// Debug Helper - Outputs that a task doesn't exist...
//
function LogDebugFuncNotExists( _task, _data, _cfg )
{
	if ( _task != TASK_PROCESS_ANCHORS && _task != TASK_PROCESS_DIVS &&  _task != TASK_PROCESS_FRAMES )
	{
		__Log( '[ TaskFuncNotExists ]' + _task + ' / ' + GetTaskName( _task ) );
	}
}


//
// Debug Helper which outputs information letting you know the script has loaded..
//
function LogDebugPreLoadInfo( _task )
{
	// Only show this data output on preload..
	if ( _task == TASK_ON_PRELOAD )
	{
		__Log( " Running " + GetActiveTag( ) + '' );
		__Log( "[ SiteSpecificFeatures ][ " + GetActiveTag( ) + " ][ " + GetTaskName( _task ) + " ] " );
	}
}


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// Creates a basic Getter and Setter function..
//
function AccessorFunc( _tab, _name, _default, _getter_prefix = 'Get' )
{
	Acecool.util.AccessorFunc( _tab, _name, _default, _getter_prefix );
}


//
// Other Definitions
//


//
// Adds a Cycle to a name
//
function AddCycle( _name = 'default', _value = 1 )
{
	SetCycle( _name, GetCycle( _name ) + _value );
}


//
// Adds a Cycle to a name
//
function SetCycle( _name = 'default', _value = 0 )
{
	if ( !__DATA.cycles[ _name ] )
		__DATA.cycles[ _name ] = { };

	console.log( ' >>>>> SET CYCLE >>>>>> ' + _value );
	console.log( ' >>>>> SET CYCLE >>>>>> ' + _value );

	__DATA.cycles[ _name ].cycle = _value;
}


//
// Returns the Cycle number for a name
//
function GetCycle( _name = 'default' )
{
	if ( !__DATA.cycles[ _name ] )
		return 0;

	return __DATA.cycles[ _name ].cycle || 0;
}


//
// Helper - Returns whether or not one cycle has been completed...
//
function HasCycledOnce( _name = 'default' )
{
	return GetCycle( _name ) > 1;
}


//
// Sets the Cycling status..
//
function SetCycling( _name = 'default', _value = false )
{
	__DATA.cycles[ _name ].cycling = _value
}


//
// Helper- Returns whether or not we're currently cycling a process...
//
function IsCycling( _name = 'default' )
{
	return GetCycling( _name );
}


//
// Map site hostname to tags - This allows several domains ( do not include sub-domain [ www / blah / download / dl ], forward slashes, or protocol [ http / https / ftp ] or special chars - only use [a-Z] _ [0-9] . )
//



//
// Tags - These are to convert domains to a common id for domains that are spread across several or who rapidly change urls.. These are to be TAG_[A-Z]+ only and the assignment should be [a-zA-Z_]+ only..
//



//
// Maps Site IDs to tasks required per site...
//


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// Site Specific Spam / Popup/under Removal...
//
function ProcessSiteSpecifcFeatures( _task, _extra = '' )
{
	var _tasks = GetActiveTasksTable( );

	//
	// Only show this data output on preload..
	if ( _task == TASK_ON_PRELOAD )
	{
		__Log( " is [ Video Site Ultimate Management ] ------------------------------------------------------------ " );
		__Log( " Host URL: " + Acecool.util.GetHost( ) );
		__Log( " Host ID: " + GetActiveTag( ) );
		__Log( " Host Processes: " + ( ( _tasks ) ? "Exist!" : "Do NOT Exist!" ) );
		__Log( " Host Tasks List: " + ( ( _tasks ) ? "Exist!" : "Do NOT Exist!" ) );

		if ( _tasks )
		{
			__Log( " Host Tasks List - Begin Output ----------------------------------------------" );

			//
			if ( ACECOOL_CFG_USING_JQUERY )
			{
				// Acecool.table.foreach( _tasks, function( _task )
				$.each( _tasks, function( _task, _value )
				{
					__Log( "[ Task List ] " + _task + " as " + GetTaskName( _task ) + " == " + _value );
				} );
			}
			else
			{

			}
			__Log( " Host Tasks List - End Output ----------------------------------------------" );
		}

		__Log( "[ ProcessSiteSpecificFeatures ]( _task: " + _task + " ie " + GetTaskName( _task ) + " )" );
	}
	else //if ( _task != TASK_PROCESS_DIVS && _task != TASK_PROCESS_ANCHORS )
	{
		//
		__Log( "[ ProcessSiteSpecificFeatures ]( _task: " + _task + " ie " + GetTaskName( _task ) + " )" );
	}

	// Grab the function, if it exists, for the current site..
	var _func = GetTaskRoutingFunc( );

	// If it exists
	if ( isfunction( _func ) )
	{
		// Determine whether ( divs / anchors ) or not ( default ) we need to apply data and recall the function for each element or to simply call it
		// The function will have a switch system within which will process the data depending on what is needed and what isn't..
		switch( _task )
		{
			//
			// For Process Divs Task, we want out function to be called for each div element..
			//
			case TASK_PROCESS_DIVS:
				//
				__Log( "	>	[ TASK_PROCESS_DIVS ]" );

				// Make sure we have a way to determine how many times this happened for instances where we only want the logic to occur the first time...
				SetCycling( 'divs', true );
				AddCycle( 'divs', 1 );

				//
				if ( ACECOOL_CFG_USING_JQUERY )
				{
					// Process Site Specific Features for each DIV element
					$( 'div' ).each( function( _, _div )
					{
						_func( _task, _div );
					} );
				}
				else
				{
					// Process Site Specific Features for each DIV element
					var _divs = document.getElementsByTagName( "div" );
					for( var i = 0; i < _divs.length; i++ )
						_func( _task, _divs[ i ] );
				}

				// Finished..
				SetCycling( 'divs', false );

				break;


			//
			// For Process anchors Task, we want out function to be called for each div element..
			//
			case TASK_PROCESS_ANCHORS:
				//
				__Log( "	>	[ TASK_PROCESS_ANCHORS ]" );

				// Make sure we have a way to determine how many times this happened for instances where we only want the logic to occur the first time...
				SetCycling( 'anchors', true );
				AddCycle( 'anchors', 1 );


				//
				if ( ACECOOL_CFG_USING_JQUERY )
				{
					// Process Site Specific Features for each A / Anchor element
					$( 'a' ).each( function( _, _anchor )
					{
						_func( _task, _anchor, _extra );
					} );
				}
				else
				{
					// Process Site Specific Features for each A / Anchor element
					var _anchors = document.getElementsByTagName( "a" );
					for( var i = 0; i < _anchors.length; i++ )
						_func( _task, _anchors[ i ], _extra );
				}

				// Finished..
				SetCycling( 'anchors', false );

				break;


			//
			// For Process iframes Task, we want out function to be called for each iframe element..
			//
			case TASK_PROCESS_FRAMES:
				//
				__Log( "	>	[ TASK_PROCESS_FRAMES ]" );

				// Make sure we have a way to determine how many times this happened for instances where we only want the logic to occur the first time...
				SetCycling( 'iframes', true );
				AddCycle( 'iframes', 1 );

				//
				if ( ACECOOL_CFG_USING_JQUERY )
				{
					// Process Site Specific Features for each A / Anchor element
					$( 'iframe' ).each( function( _, _iframe )
					{
						_func( _task, _iframe );
					} );
				}
				else
				{
					// Process Site Specific Features for each A / Anchor element
					var _iframes = document.getElementsByTagName( "iframe" );
					for( var i = 0; i < _iframes.length; i++ )
						_func( _task, _iframes[ i ] );
				}

				// Finished..
				SetCycling( 'iframes', false );

				break;

			// Most Taskes will simply be executed..
			default:
				//
				__Log( "	>	[ DEFAULT ]" );

				//
				_func( _task, null );
		}
	}
}


//
// MAIN
//
function __main_ultimate_site_manager_script__( _task, _data )
{
	//
	if ( !HasCycledOnce( 'SetupConfig' ) )
		SetupConfiguration( );

	// Then, for each of those we also need to see if we're supposed to process the divs / anchors...
	var _tasks = GetActiveTasksTable( );

	// Debugging - Log the pre-action
	__Log( "[ Determining if current site has task enabled ]: " + GetTaskName( _task ) );

	// If _tasks is defined... and we aren't using one of our processing tasks ( as those are called anyway )
	if ( _tasks && ( _task !== TASK_PROCESS_DIVS && _task !== TASK_PROCESS_ANCHORS && _task !== TASK_PROCESS_FRAMES && _task !== TASK_SETUP_OBSERVER ) )
	{
		// If the task is enabled...
		if ( _tasks[ _task ] )
		{
			// Debugging - Log this action!
			__Log( "[ TASK SUPPORTED ]: " + GetTaskName( _task ) );

			// Primary call - runs the default tasks for on preload, load, ready, frame load..
			ProcessSiteSpecifcFeatures( _task );


			//
			// Note: May alter TASK_PROCESS_* to HANDLER_PROCESS_DIVS so TASKS are for ON events and HANDLERS can be used within those events...
			//
			// // If Divs task is defined to be executed on this site... run it..
			// if ( _tasks[ TASK_PROCESS_DIVS ] )
			// 	ProcessSiteSpecifcFeatures( TASK_PROCESS_DIVS );

			// // If Anchors task is defined to be executed on this site... run it..
			// if ( _tasks[ TASK_PROCESS_ANCHORS ] )
			// 	ProcessSiteSpecifcFeatures( TASK_PROCESS_ANCHORS );

			//
			// TODO: Determine whether or not I want to limit use of Processes during tasks - ie TASK_ON_PRELOAD must be active to run PROCESS_DIVS - this would prevent the divs process from running all the time... - Right now I will prevent them from running outside of tasks....
			//


			//
			//
			//

			// If Divs task is defined to be executed on this site... run it..
			if ( _tasks[ TASK_PROCESS_DIVS ] )
				ProcessSiteSpecifcFeatures( TASK_PROCESS_DIVS );


			//
			// Anchors
			//

			// If Anchors task is defined to be executed on this site... run it..
			if ( _tasks[ TASK_PRE_PROCESS_ANCHORS ] )
				ProcessSiteSpecifcFeatures( TASK_PRE_PROCESS_ANCHORS );

			if ( _tasks[ TASK_PROCESS_ANCHORS ] )
				ProcessSiteSpecifcFeatures( TASK_PROCESS_ANCHORS );

			// if ( _tasks[ TASK_POST_PROCESS_ANCHORS ] )
			// 	setTimeout( GetTaskFunc( TASK_POST_PROCESS_ANCHORS ), 1000 )


			//
			//
			//

			// If IFrames task is defined to be executed on this site... run it..
			if ( _tasks[ TASK_PROCESS_FRAMES ] )
				ProcessSiteSpecifcFeatures( TASK_PROCESS_FRAMES );


			//
			//
			//

			// If we have a task to setup the observer... run it..
			if ( _tasks[ TASK_SETUP_OBSERVER ] )
				ProcessSiteSpecifcFeatures( TASK_SETUP_OBSERVER );
		}
		else
		{
			__Log( "[ !!! TASK DISABLED !!! ]: " + GetTaskName( _task ) );
		}
	}
}


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//


//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//																																													//
// Divider																																											//
//																																													//
// ################################################################################################################################################################################ //
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
// ################################################################################################################################################################################ //
//